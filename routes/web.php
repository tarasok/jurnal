<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

Route::get('/', 'PostsController@index')->name('main');
Route::get('search', 'PostsController@search')->name('search');
Route::get('category/{id}', 'PostsController@category')->name('category');
Route::get('user/{id}', 'PostsController@user')->name('user');
Route::get('tag/{id}', 'PostsController@tag')->name('tag');
Route::get('users', 'UsersController@index')->name('users');
Route::get('api/post', 'PostsController@api')->name('api.post');
Route::get('api/post/all', 'PostsController@all');

Route::post('api/count/view', 'CountController@view');
Route::post('api/count/share', 'CountController@share');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('api/image', 'ImageController');
    Route::get('profile', 'ProfileController@show')->name('profile');
});

Route::get('post/{id}', ['as' => 'post', 'uses' => 'PostController@show']);

Route::resource('item', 'ItemController');
Route::get('items/{ids}', ['as' => 'catalog', 'uses' => 'CatalogController@index']);

Route::get('sitemap.xml', 'XmlController@sitemap');
Route::get('sync/{subject}/{id}', ['as' => 'sync.post', 'uses' => 'SyncController@index']);

Route::get('phpinfo', ['middleware' => 'auth', 'uses' => 'Admin\UtilitiesController@phpinfo']);

Route::group(['middleware' => 'auth', 'namespace' => 'Admin', 'prefix' => 'admin'], function()
{
    Route::resource('urls', 'UrlController');
    Route::post('urls/{id}/update', 'UrlController@update')->name('admin.urls.update');
    Route::get('urls/{id}/delete', 'UrlController@destroy')->name('admin.urls.destroy');
    
    Route::resource('users', 'UserController');
    Route::resource('posts', 'PostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('products', 'ProductController');
    Route::resource('shops', 'ShopController');
    Route::get('shops/{id}/price', 'ShopController@priceIndex')->name('admin.shops.price');
    Route::get('shops/{id}/price/update', 'ShopController@priceUpdate')->name('admin.shops.price.update');
    Route::get('shops/{id}/price/import', 'ShopController@priceImport')->name('admin.shops.price.import');

    Route::resource('tags', 'TagController');
    Route::post('tags/{id}/update', 'TagController@update')->name('admin.tags.update');

    Route::resource('images', 'ImageController');
});

Route::get('/{slug}', ['as' => 'slug','uses' => 'SlugController@index']);
