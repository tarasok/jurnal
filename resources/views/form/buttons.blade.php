<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <div class="btn-group">
            <a class="btn btn-default" href="{{ Request::input('return') }}"><span class="glyphicon glyphicon-arrow-left"></span> Отмена</a>
            <button type="submit" class="btn btn-default" name="return" value="{{ Request::input('return') }}"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</button>
            @if($method == 'PUT')
                <button type="submit" class="btn btn-default" name="return" value="{{ URL::full() }}"><span class="glyphicon glyphicon-refresh"></span> Обновить</button>
            @endif
        </div>
    </div>
</div>