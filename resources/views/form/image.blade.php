<div class="form-group">
    {!! Form::label($key, null, ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-8">
        <button class="btn btn-default btn-image-upload pull-left" data-key="{{ $key }}" data-target="#{{ $type }}" data-type="{{ $type }}"><span class="glyphicon glyphicon-upload"></span> Загрузить</button>
        <span id="{{ $type }}">
            @if($id)
                <button class="btn btn-default btn-image-upload-delete pull-left"><span class="glyphicon glyphicon-trash"></span> Удалить</button>
                <img src="{{ $image->url_thumb }}" alt="" />
                <input type="hidden" name="{{ $key }}" value="{{ $id }}" />
            @endif
        </span>
    </div>
</div>