<div class="form-group">
    {!! Form::label($key, null, ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-8">
        <select class="form-control" name="{{ $key }}">
            @foreach($options as $label => $option)
                <option value="{{ $option }}"{{ $option == $value?'selected="selected"':'' }}>{{ $label }}</option>
            @endforeach
        </select>
    </div>
</div>