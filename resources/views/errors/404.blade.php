<!DOCTYPE html>
<html>
<head>
    <title>Страница не существует.</title>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:400,700&subset=latin,cyrillic" rel="stylesheet"
          type="text/css">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Exo 2', sans-serif;
            font-size: 28px;
        }

        a {
            color: #B0BEC5;
            text-decoration: none;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 200px;
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">404</div>
        <p>Страница не существует :-(</p>
        <p><a href="/">Mebeljurnal.ru</a></p>
    </div>
</div>
</body>
</html>
