@extends('layouts.master')

@section('content')
<div class="container">
    <br/>
    <h1 class="mb-20">Страница не существует</h1>
    <div class="alert alert-warning alert-dismissable">
	Извините, данный раздел находится в разработке :-(
    </div>
</div>
@endsection