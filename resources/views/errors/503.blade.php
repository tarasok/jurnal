<!DOCTYPE html>
<html>
<head>
    <title>Ведутся работы!</title>

    <link href="https://fonts.googleapis.com/css?family=Exo+2:400,700&subset=latin,cyrillic" rel="stylesheet"
          type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-family: 'Exo 2', sans-serif;
            font-size: 18px;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Ведутся работы!</div>
        <div>Возвращайтесь через 15 минут...</div>
    </div>
</div>
</body>
</html>
