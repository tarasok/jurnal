@extends('layouts.master',[
    'metatitle'=>'Поиск: '.$q
])

@section('content')
    <div id="articles">
	<div class="container title text-center">
	    <h1>Поиск: {{ $q }}</h1>
	</div>
	@include('parts.posts')
    </div>
@endsection