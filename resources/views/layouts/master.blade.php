<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ 'Мебельный журнал' }} - Mebeljurnal.ru</title>
  <meta property="og:title" content="{{ 'Мебельный журнал' }}">
  <meta name="description" content="{{ (isset($description) and $description) ? $description : 'Лучшие идеи для творчества, советы и рекомендации специалистов, фото/видео обзоры, а также масса другой полезной информации по выбору и покупке мебели.' }}" />
  <meta property="og:description" content="{{ (isset($description) and $description) ? $description : 'Лучшие идеи для творчества, советы и рекомендации специалистов, фото/видео обзоры, а также масса другой полезной информации по выбору и покупке мебели.' }}">
  <meta property="og:image" content="{{ url('/images/blog_03/group_00/624/mebelnyj-zhurnal-s-besplatnoj-publikatsiej-onlajn-forma-preview.jpg') }}">
  <link href="{{ url('/images/blog_03/group_00/624/mebelnyj-zhurnal-s-besplatnoj-publikatsiej-onlajn-forma-preview.jpg') }}" rel="image_src" />
  @if(!($robots ?? false))
  <meta name="robots" content="noindex, nofollow">
  @endIf
  @if(isset($author))
  <meta name="author" content="{{ $author }}">
  @endIf
  @if(isset($url))
  <link href="{{ $url }}" rel="canonical"/>
  @endIf
  <link href="{{ url('favicon.ico') }}" rel="shortcut icon"/>
  <link href="{{ asset('vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('vendors/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ asset('vendors/magnific-popup/magnific-popup.css') }}" rel="stylesheet">
  <link href="{{ elixir('css/site.css') }}" rel="stylesheet"/>
  @stack('style')
 </head>
  <body{!! isset($body)?' class="'.$body.'"':'' !!}>
    @include('parts.nav')
  	@include('parts.alerts.message')
  	@include('parts.alerts.error')
    @yield('content') 
    <footer>
	<div class="container">
	    <ul class="menu">
		<li>&copy; 2009-{{ date('Y') }}, <a href="{{ url('/') }}">Mebeljurnal.ru</a></li>
		<li>Время выполнения {{ round(microtime(true) - LARAVEL_START, 3) }} </li>
	    </ul>
	    <div class="like">
		<div class="ya-share2" data-services="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus" data-counter="" ></div>			
	    </div>
	</div>
    </footer>
	<script src="{{ elixir('js/vendors.js') }}"></script>
    <script src="{{ elixir('js/site.js') }}" type="text/javascript"></script>
    <script src="//yastatic.net/share2/share.js"></script>
    @stack('script')
    @include('parts.counters')
  </body>
</html>