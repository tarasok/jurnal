<div class="container items">
    @if(count($posts))
    <div class="row">
	@foreach($posts as $i=>$post)
	    @include('parts.post', ['class'=>'col-md-4'])
	@if (($i+1)%3==0)
	<div class="clearfix visible-md visible-lg"></div>
	@endif
	@endForeach
    </div>
    @else
    <div class="alert alert-warning alert-dismissable">
	По вашему запросу ничего не нашлось
    </div>
    @endif
</div>
@if($posts->lastPage()>1)
<div class="text-center">
    <button id="load" class="btn btn-default btn-posts mb-30" data-url="{{ $next_page_url }}" data-loading-text="Загружается...">Показать ещё</button>
</div>
@endif
<div class="banner">
@include('ads.g1')
</div>

@push('script')
<script>
    jQuery(document).ready(function($) {
	jQuery(document).on('click','#load', function(){
	    var btn = $(this);
	    btn.button('loading')
	    $.ajax({
		url: btn.data('url'),
		dataType: "json",
		success: function(json){
		    json.data.forEach(function(post,i){
			html =	'<article class="col-md-4">'+
				'   <div class="image">'+
				'	<a href="'+post.url+'"><img src="'+post.thumb+'" alt="'+post.title+'" class="img-responsive"></a>'+
				'   </div>'+
				'   <div class="text">'+
				'	<ul class="list-inline details">'+
				'	    <li>'+post.date+'</li>'+
				'	    <li><span class="glyphicon glyphicon-eye-open"></span> '+post.view_count+'</li>'+
				'	    <li><span class="glyphicon glyphicon-star-empty"></span> '+post.view_avg+'</li>'+
				'	    <li><span class="glyphicon glyphicon-thumbs-up"></span> '+post.share_count+'</li>'+
				'	    <li><span class="glyphicon glyphicon-camera"></span> '+post.photo_count+'</li>'+
				'	</ul>'+
				'	<div class="title">'+
				'	    <a href="'+post.url+'">'+post.title+'</a>'+
				'	</div>'+
				'   </div>'+
				'   <div class="category">'+
				'	<a href="'+post.category.url+'" class="category-link">'+post.category.title+'</a>'+
				'   </div>'+
				'</article>';
			if ((i+1)%3==0){
			    html = html + '<div class="clearfix visible-md visible-lg"></div>';
			}
			$(".container.items .row").append(html);
		    });
		    console.log(json);
		    btn.data('url',json.next_page_url)
		    btn.button('reset');
		    if(btn.data('url') == null){
			btn.hide();
		    }
		}
	    });
	});
    });
</script>
@endpush