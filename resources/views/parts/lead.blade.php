<div class="container lead_items">
    <div class="row">
	@include('parts.post', [
	    'post' => $posts[0],
	    'class'=>'col-md-8 col-lg-6 style1-md style1-lg',
	])
	@include('parts.post', [
	    'post' => $posts[1],
	    'class'=>'col-md-4 col-lg-3 hidden-md style2-lg',
	    'view_intro'=>true,
	])
	<div class="col-md-4 col-lg-3 visible-md visible-lg">
	    @include('ads.send')
	</div>
	<div class="clearfix visible-md visible-lg"></div>
	@include('parts.post', [
	    'post'=>$posts[2],
	    'class'=>'col-md-4 col-lg-3 style2-md style2-lg',
	    'view_intro'=>true,
	])
	@include('parts.post', [
	    'post'=>$posts[3],
	    'class'=>'col-md-4 col-lg-6 style1-lg style2-md',
	])
	<div class="col-md-4 col-lg-3 business">
	    @include('parts.business')
	</div>
	<div class="clearfix visible-md visible-lg"></div>
	<?php for ($i=4; $i<=6; $i++) { ?>
	@include('parts.post', [
	    'post'=>$posts[$i],
	    'class'=>'col-lg-3 col-md-4',
	])
	<?php } ?>
	@include('parts.post', [
	    'post'=>$posts[7],
	    'class'=>'col-md-4 col-lg-3 hidden-md',
	    'view_intro'=>true,
	])
    </div>
</div>