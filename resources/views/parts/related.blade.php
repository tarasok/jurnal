<div class="articles">
    <div class="container">
	<div class="h3">Читайте также:</div>
	<div class="row">
	    @foreach($posts as $i => $post)
		<article class="col-md-4">
		    <div class="image">
			<a href="{{ $post->url }}"><img src="{{ $post->thumb }}" class="img-responsive" /></a>
		    </div>
		    <div class="text">
			<div class="title">
				<a href="{{ $post->url }}">{{ $post->title }}</a>
			</div>
			<div class="category">
			    <a href="{{ route('slug',$post->category->alias) }}" class="category-link">{{ $post->category->title }}</a>
			</div>
		    </div>
		</article>
		@if (($i+1)%3==0)
		<div class="clearfix visible-md visible-lg"></div>
		@endif
	    @endforeach
	</div>
    </div>
</div>