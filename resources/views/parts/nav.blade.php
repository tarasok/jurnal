<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
	<div class="navbar-header">
	    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	    </button>
	    <span class="hidden-md hidden-lg">
		<a class="btn navbar-btn btn-link pull-right" href="/send.html"><span class="glyphicon glyphicon-edit"></span><span class="hidden-xs"> Добавить</span></a>
		<a class="btn btn-link navbar-btn pull-right" href="#searchbar" data-toggle="collapse"><span class="glyphicon glyphicon-search"></span></a>
	    </span>
	    <a class="navbar-brand" href="{{ url('/') }}" >Мебельный <span>журнал</span></a>
	</div>
	<div id="navbar" class="collapse navbar-collapse">
	    <ul class="nav navbar-nav">
		<li class="dropdown">
		    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Категории<b class="caret"></b></a>
		    <div class="dropdown-menu navbar-menu">
		    @include('parts.categories')
		    </div>
		</li>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">Онлайн конструкторhh <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a href="https://kitchen.prodboard.com/SR_85c532c24af444d797a58855b3848192_0019/STANDART" target="_blank">Кухня</a></li>
				<li><a href="https://kitchen.prodboard.com/SR_85c532c24af444d797a58855b3848192_0019/VMNY" target="_blank">Шкаф-купе</a></li>
			</ul>
		</li>
	    </ul>
	    <ul class="nav navbar-nav navbar-right hidden-xs hidden-sm">
		<li><a href="#searchbar" data-toggle="collapse"><span class="glyphicon glyphicon-search"></span> Поиск</a></li>
	    </ul>
	    <ul class="nav navbar-nav navbar-right hidden-md">
	    @if (Auth::guest())
		<li>
			<a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span> Войти</a>
		</li>
	    @else
		<li class="dropdown">
		    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }} <b class="caret"></b></a>
		    <ul class="dropdown-menu">
                <li><a href="{{ action("Admin\UrlController@index") }}"><span class="glyphicon glyphicon-link"></span>&nbsp; Ссылки</a></li>
                <li><a href="{{ route("admin.users.index") }}"><span class="glyphicon glyphicon-user"></span>&nbsp; Пользователи</a></li>
                <li><a href="{{ route("admin.posts.index") }}"><span class="glyphicon glyphicon-file"></span>&nbsp; Посты</a></li>
                <li><a href="{{ route("admin.categories.index") }}"><span class="glyphicon glyphicon-folder-open"></span>&nbsp; Категории</a></li>
                <li><a href="{{ route("admin.tags.index") }}"><span class="glyphicon glyphicon-tags"></span>&nbsp; Тэги</a></li>
                <li><a href="{{ route("admin.images.index") }}"><span class="glyphicon glyphicon-picture"></span>&nbsp; Изображения</a></li>
                <li><a href="{{ route("admin.shops.index") }}"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp; Магазины</a></li>
                <li><a href="{{ route("admin.products.index") }}"><span class="glyphicon glyphicon-lamp"></span>&nbsp; Товары</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ action("Auth\AuthController@logout") }}"><span class="glyphicon glyphicon-log-out"></span>&nbsp; Выход</a></li>
		    </ul>
		</li>
	    @endif
	    </ul>
	    <a class="btn navbar-btn btn-primary btn-sm pull-right hidden-xs hidden-sm" href="{{ \App\Post::url(624) }}"><span class="glyphicon glyphicon-file"></span> Добавить</a>
	</div>
    </div>
</nav>
<nav id="searchbar" class="navbar navbar-default navbar-fixed-top collapse">
    <div class="container">
	<p class="navbar-text hidden-xs">Поиск по сайту</p>	
	<a href="#searchbar" class="close" data-toggle="collapse">&times;</a>
	<form class="navbar-form" role="search" action="{{ route('search') }}">
	    <div class="input-group">
		<input type="text" class="form-control" placeholder="Поиск" name="q" value="{{ request()->input('q') }}">
		<span class="input-group-btn">
			<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
		</span>
	    </div>
	</form>
    </div>
</nav>