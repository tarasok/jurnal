<article class="{{ $class ?? 'default' }}">
    <div class="image">
	<a href="{{ $post->url }}"><img src="{{ $post->thumb }}" alt="{{ $post->title }}" class="img-responsive"></a>
    </div>
    <div class="text">
	<ul class="list-inline details">
	    <li>{{ $post->date }}</li>
	    <li><span class="glyphicon glyphicon-eye-open"></span> {{ $post->view_count }}</li>
	    <li><span class="glyphicon glyphicon-star-empty"></span> {{ $post->view_avg }}</li>
	    <li><span class="glyphicon glyphicon-thumbs-up"></span> {{ $post->share_count }}</li>
	    <li><span class="glyphicon glyphicon-camera"></span> {{ $post->photo_count }}</li>
	</ul>
	<div class="title">
	    <a href="{{ $post->url }}">{{ $post->title }}</a>
	</div>
	@if(isset($view_intro) and $view_intro)
	<div class="intro">
	    {{ $post->description }}
	</div>
	@endIf
    </div>
    <div class="category">
	<a href="{{ $post->category->url }}" class="category-link">{{ $post->category->title }}</a>
    </div>		
</article>