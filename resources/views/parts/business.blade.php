<div class="cover">
	<div class="header">
		<a href="{{ \App\Category::url(100) }}">Мебельный бизнес</a>
	</div>
	@if($posts)
	    @foreach($posts as $post)
		<article>
		    <div class="date">
			{{ $post->date }}
		    </div>
		    <div class="title">
			<a href="{{ $post->url }}">{{ $post->title }}</a>
		    </div>
		</article>
	    @endforeach
	@endif
	<div class="more">
		<a href="{{ \App\Category::url(100) }}">Ещё статьи о мебельном бизнесе</a>
	</div>
</div>