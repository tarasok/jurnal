<!-- Shares -->
<div class="ya-share2" data-services="vkontakte,facebook,twitter,odnoklassniki,moimir" data-title="{{ $post->title }}" data-image="{{ $post->thumb }}" data-url="{{ $post->url }}" data-counter="" ></div>
 <!-- Comments -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>
<script type="text/javascript">VK.init({apiId: 5800673, onlyWidgets: true});</script>
<div id="vk_comments" class="mb-20"></div>
<script type="text/javascript">
	VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"}, 'post_{{ $post->id }}');
</script>