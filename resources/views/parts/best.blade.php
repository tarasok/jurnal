<div class="best">
    <h1>Самые читаемые <span class="glyphicon glyphicon-fire"></span></h1>
    <div class="container">
	<div class="row">
	    <div class="owl-carousel">
	    @if($posts)
		@foreach($posts as $post)
			@include('parts.post')
		@endforeach
	    @endif
	    </div>
	</div>
    </div>
    <div class="owl-buttons">
	<a href="#" class="owl-prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a href="#"  class="owl-next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    <div class="banner">
	@include('ads.g1')
    </div>
</div>