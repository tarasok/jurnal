@if($title)
	<h3><span class="hidden-xs">Смотрите также: </span><a href=http://palmiramebel.ru/items.html?{{ $select }}>{{ $title }}</a></h3>
@endif
<div class="slider row">
@foreach($items as $item)
	<div class="col-md-3 col-xs-6">
	<a href="{{ $item->url }}" class="item" target="_blank"><img src="{{ $item->thumb }}" alt="{{ $item->title }}" title="{{ $item->title }}" /></a>
	</div>
@endforeach
</div>