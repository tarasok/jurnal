<ul class="list-inline">
    @foreach($categories as $category)
    <li>
        <a href="{{ $category['url'] }}" class="btn btn-default">{{ $category['title'] }}</a>
    </li>
    @endForeach
    <li>
        <a href="{{ \App\Category::url(100) }}" class="btn btn-default">Мебельный бизнес</a>
    </li>
</ul>