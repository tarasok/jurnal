<?php if (Session::get('message')){ ?>
<div class="alert alert-success fade in">
    <div class="container">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo Session::get('message') ?>
    </div>
</div>
<?php } ?>