<?php if (Session::get('error')){ ?>
<div class="alert alert-danger fade in">
    <div class="container">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo Session::get('error') ?>
	</div>
</div>
<?php } ?>