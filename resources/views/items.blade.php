@extends('layouts.master', [
    'metatitle' => $title,
    //'description' => $category->description,
    //'url' => $category->url,
    //'image_url' => isset($posts[0]) ? $posts[0]->thumb : null,
    //'robots' => true
])

@section('content')
    <div id="articles">
	<div class="container title text-center">
	    <h1>{{ $title }}</h1>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="row">
				@foreach($items as $item)
					<div class="col-md-3 col-xs-6 mb-30">
						<a href="{{ $item->url }}" target="_blank"><img src="{{ $item->thumb }}" alt="{{ $item->title }}" title="{{ $item->title }}" class="img-responsive" /></a>
					</div>
				@endforeach
				</div>
			</div>
			<div class="col-md-3">
				<div class="list-group">
					@foreach($options_menu as $option)
					<a href="{{ route('catalog', $option->id) }}" class="list-group-item{{ $current_id==$option->id?' active':'' }}">{{ $option->title }}</a>
					@endforeach
				</div>
			</div>
		</div>
	</div>
    </div>
@endsection