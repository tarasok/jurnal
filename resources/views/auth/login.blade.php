@extends('layouts.master', [
    'metatitle' => 'Войти',
    'body' => 'auth',
])

@section('content')
<div class="container">
    <form id="form-auth" class="form-signin" role="form" action="{{ action("Auth\LoginController@login") }}">
	<h2 class="form-signin-heading text-center">Войдите!</h2>
	<div class="form-group">
	    <input type="text" class="form-control" placeholder="E-mail" name="email" value="" autofocus />
	</div>
	<div class="form-group">
	    <input type="password" class="form-control" placeholder="Пароль" name="password" />
	</div>
	<div class="checkbox text-center mb-15">
	    <label>
	      <input type="checkbox" name="remember" value="yes" checked="checked" alt="Запомнить меня"> Запомнить меня
	    </label>
	</div>
	{!! csrf_field() !!}
	<input type="hidden" name="return" value="{{ Url()->previous() }}" />
	<button class="btn btn-lg btn-primary btn-block mb-5" type="submit" data-loading-text="Проверка...">Войти</button>
	<div class="text-center mb-5">
	    <a class="btn btn-link" href="{{ action('Auth\ResetPasswordController@reset') }}">Вспомнить пароль?</a>
	</div>
    </form>
</div>
@endsection