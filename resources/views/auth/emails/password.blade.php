<p>Забыли пароль? Нажмите ссылку, чтобы сменить пароль:</p>
<p><a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </p>
