@extends('layouts.master', [
    'metatitle' => 'Сброс пароля',
    'body' => 'auth',
])

@section('content')
<div class="container">
    <form id="form-auth" class="form-signin" role="form" action="{{ action("Auth\ForgotPasswordController@sendResetLinkEmail") }}">
	<h2 class="form-signin-heading text-center">Сброс пароля</h2>
	<div class="form-group">
	    <input type="text" class="form-control" placeholder="E-mail" name="email" value="" autofocus />
	</div>
	{!! csrf_field() !!}
	<button class="btn btn-lg btn-primary btn-block mb-15" type="submit" data-loading-text="Проверка...">Отправить</button>
    </form>
</div>
@endsection
