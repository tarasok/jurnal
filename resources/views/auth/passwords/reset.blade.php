@extends('layouts.master', [
    'metatitle' => 'Сбросить пароль',
    'body' => 'auth',
])

@section('content')
<div class="container">
    <form id="form-auth" class="form-signin" role="form" action="{{ action("Auth\PasswordController@reset") }}">
	<h2 class="form-signin-heading text-center">Сбросить пароль</h2>
	<div class="form-group">
	    <input type="text" class="form-control" placeholder="E-mail" name="email" value="{{ $email or old('email') }}" />
	</div>
	<div class="form-group">
	    <input type="password" class="form-control" placeholder="Новый пароль" name="password" autofocus />
	</div>
	<div class="form-group">
	    <input type="password" class="form-control" placeholder="Подтверждение пароля" name="password_confirmation" />
	</div>
	{!! csrf_field() !!}
	<input type="hidden" name="token" value="{{ $token }}">
	<button class="btn btn-lg btn-primary btn-block mb-15" type="submit" data-loading-text="Проверка...">Сохранить</button>
    </form>
</div>
@endsection
