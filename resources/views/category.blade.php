@extends('layouts.master', [
    'metatitle' => $category->meta_title,
    'description' => $category->description,
    'url' => $category->url,
    'image_url' => isset($posts[0]) ? $posts[0]->thumb : null,
    'robots' => true
])

@section('content')
    <div id="articles">
        @if($category->image_full)
        <div class="jumbotron jumbotron-title"{!! $category->image_full ? ' data-image="'. url($category->image_full).'"':'' !!}>
            <div class="container">
                <h1>{!! $category->header_title !!}</h1>
                <ul class="list-inline counter">
                    <li><span class="glyphicon glyphicon-eye-open"></span> 219 420</li>
                    <li><span class="glyphicon glyphicon-star-empty"></span> 282</li>
                    <li><span class="glyphicon glyphicon-thumbs-up"></span> 99</li>
                </ul>
            </div>
        </div>
        @else
        <div class="container title">
            <h1>{{ $category->title }}</h1>
        </div>
        @endif
        @if($category->text)
        <div class="container description">
            {!! $category->text !!}
        </div>
        @endif
        @include('parts.posts')
    </div>
@endsection

@push('script')
<script>
	$.post("{{ action('CountController@view')}}","id={{$category->id}}&type=Category&_token={{ csrf_token() }}");
</script>
@endpush