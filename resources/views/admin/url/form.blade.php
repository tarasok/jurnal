@extends('layouts.master',[
    'metatitle'=>'Редактировать ссылку',
])

@section('content')
	<div class="container">
	    <h1>Редактировать ссылку</h1>
        <form class="form-horizontal" method="post" action="{{ route('admin.urls.update',$url->id) }}">
            @foreach($url->getFillable() as $name)
                @if($name == 'type')
                <div class="form-group">
                    <label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="{{ $name }}">
                          @foreach(['Нет редиректа'=>'','Редирект на пост'=>'post','Редирект на категорию'=>'category','Редирект на пользователя'=>'user'] as $label => $value)
                          <option value="{{ $value }}"{{ $value==$url->{$name}?'selected="selected"':'' }}>{{ $label }}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                @elseif($name == 'status')
                    <div class="form-group">
                        <label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="{{ $name }}">
                                @foreach(['Не проверено'=>0,'Проверено'=>1] as $label => $value)
                                    <option value="{{ $value }}"{{ $value==$url->{$name}?'selected="selected"':'' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @else
                <div class="form-group">
                    <label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="{{ $name }}" name="{{ $name }}" value="{{ $url->{$name} }}">
                    </div>
                </div>
                @endif
            @endforeach
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="btn-group">
						<a class="btn btn-default" href="{{ Request::input('return') }}"><span class="glyphicon glyphicon-arrow-left"></span> Отмена</a>
						<button type="submit" class="btn btn-default" name="return" value="{{ Request::input('return') }}"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</button>
						<button type="submit" class="btn btn-default" name="return" value="{{ URL::full() }}"><span class="glyphicon glyphicon-refresh"></span> Обновить</button>
					</div>
				</div>
			</div>
            {!! csrf_field() !!}
        </form>
	</div>
@endsection