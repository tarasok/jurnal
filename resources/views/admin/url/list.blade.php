@extends('layouts.master',[
    'metatitle'=>'Неактуальные ссылки',
])

@section('content')
	<div class="container">
	    <h1>Неактуальные ссылки</h1>
	    <table class="table table-bordered table-hover small">
		<tr>
		    <th>Неактуальная ссылка</th>
		    <th>На странице</th>
		    <th>Переадресация</th>
		    <th>Кол</th>
            <th width="12"></th>
		    <th width="12"></th>
		</tr>
		@foreach($urls as $url)
		<tr>
		    <td>{{ $url->url }}</td>
		    <td><a href="{{ $url->referer }}" target="_blank">{{ $url->referer }}</a></td>
		    <td>{{ $url->redirect }}</td>
		    <td>{{ $url->view_count }}</td>
            <td><a href="{{ route('admin.urls.show',array('id'=>$url->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
            <td><a href="{{ route('admin.urls.destroy',array('id'=>$url->id, 'return'=>URL::full())) }}" class="link-confirm"><span class="glyphicon glyphicon-trash"></span></a></td>
		</tr>
		@endforeach
	    </table>
	</div>
@endsection