@extends('layouts.master',[
    'metatitle'=>'Изображения',
])

@section('content')
	<div class="container">
	    <h1>Изображения</h1>
	    <table class="table table-bordered table-hover small">
		<tr>
		    <th width="50">id</th>
		    <th width="100">Изображеие</th>
		    <th width="120">Тип</th>
		    <th></th>
		    <th width="140"></th>
		    <th width="20"></th>
		</tr>
		@foreach($images as $image)
		<tr>
            <td align="right">{{ $image->id }}</td>
            <td><img src="{{ $image->url_thumb }}" alt="" /></td>
            <td>{{ $image->type }}</td>
            <td>
                <p>
                    Пользователь: {{ $image->user->name }}
                    Формат: {{ $image->extention }}
                    Папка: {{ $image->path }}
                </p>
                <ul class="list-unstyled">
                    @foreach($image->versions_config as $versions_name => $vc)
						@if(isset($image->url->{$versions_name}))
                        <li><b><a href="{{ $image->url->{$versions_name} }}" class="gallery-item">{{ $versions_name }}</a></b>: {{ implode(', ',$vc) }}</li>
                    	@endif
					@endforeach
                </ul>
            </td>
            <td>{{ $image->updated_at }}</td>
            <td>
				@if($image->is_related)
					<span class="glyphicon glyphicon-ok text-success"></span>
				@else
					<span class="glyphicon glyphicon-remove text-warning"></span>
				@endif
			</td>
		</tr>
		@endforeach
	    </table>
	</div>
@endsection