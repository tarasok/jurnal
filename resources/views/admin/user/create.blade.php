@extends('layouts.master',[
    'metatitle'=>'Создать пользователя',
])

@section('content')
	<div class="container">
	    <h1>Создать пользователя</h1>
		@include('admin.user.form', [
		    			'action' => route('admin.users.store'),
		    			'method' => 'POST',
		    		])
	</div>
@endsection

@push('style')
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush
