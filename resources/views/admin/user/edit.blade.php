@extends('layouts.master',[
    'metatitle'=>'Редактировать Пользователя',
])

@section('content')
	<div class="container">
	    <h1>Редактировать пользователя</h1>
		@include('admin.user.form', [
		    			'action' => route('admin.users.update',$user->id),
		    			'method' => 'PUT',
		    		])
	</div>
@endsection

@push('style')
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush

