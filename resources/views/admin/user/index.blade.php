@extends('layouts.master',[
    'metatitle'=>'Пользователи',
])

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h1>Пользователи <span class="badge">{{ $users->total() }}</span></h1>
			</div>
			<div class="col-md-2 btn-admin-group">
				<a class="btn btn-default btn-block" href="{{ route('admin.users.create',array('return'=>URL::full())) }}"><span class="glyphicon glyphicon-plus"></span> Создать</a>
			</div>
		</div>
	    <table class="table table-bordered table-hover table-striped">
		<tr>
            <th class="text-center">id</th>
		    <th width="50">Фото</th>
		    <th>Имя</th>
		    <th class="text-center"><span class="glyphicon glyphicon-star-empty"></span></th>
            <th class="text-center"><span class="glyphicon glyphicon-thumbs-up"></span></th>
            <th class="text-center"><span class="glyphicon glyphicon-eye-open"></span></th>
            <th class="text-center"><span class="glyphicon glyphicon-pencil"></span></th>
            <th class="text-center"><span class="glyphicon glyphicon-camera"></span></th>
            <th class="text-center"><span class="glyphicon glyphicon-facetime-video"></span></th>
		    <th>Почта</th>
		    <th width="10"></th>
		</tr>
		@foreach($users as $user)
		<tr>
			<td align="right">{{ $user->id }}</td>
			<td>@if($user->avatar)<img src="{{ $user->avatar->url->small  }}" alt="" />@endif</td>
			<td>
                <a href="{{ $user->url }}" target="_blank">{{ $user->name }}</a>
                <div class="small text-muted">
                    {{ $user->alias }}
                </div>
            </td>
		    <td align="right">{{ $user->rating }}</td>
            <td align="right">{{ $user->share_count }}</td>
            <td align="right">{{ $user->view_avg }}</td>
            <td align="right">{{ $user->post_count }}</td>
            <td align="right">{{ $user->photo_count }}</td>
            <td align="right">{{ $user->video_count }}</td>
		    <td>{{ $user->email }}</td>
            <td><a href="{{ route('admin.users.edit',array('id'=>$user->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
		</tr>
		@endforeach
	    </table>
        <div class="text-center">
            {{ $users->render() }}
        </div>
	</div>
@endsection

@push('style')
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush