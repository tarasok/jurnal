<form class="form-horizontal" action="{{$action}}" method="POST" enctype="multipart/form-data">
	@if($method == 'PUT')
		<input name="_method" type="hidden" value="PUT">
	@endif
	@foreach($user->getFillable() as $name)
		@if($name == 'description')
			<div class="form-group">
				<label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
				<div class="col-sm-8">
					<textarea class="form-control" id="{{ $name }}" name="{{ $name }}" rows="3">{{ $user->{$name} }}</textarea>
				</div>
			</div>
		@elseif($name == 'without_ad')
			<div class="form-group">
				<label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
				<div class="col-sm-8">
					<select class="form-control" name="{{ $name }}">
						@foreach(['Отображать рекламу'=>0,'Без рекламы'=>1] as $label => $value)
							<option value="{{ $value }}"{{ $value == $user->{$name}?'selected="selected"':'' }}>{{ $label }}</option>
						@endforeach
					</select>
				</div>
			</div>
		@elseif($name == 'avatar_id')
			@include('form.image',['key'=>'avatar_id','id'=>$user->avatar_id,'image'=>$user->avatar,'type'=>'user_avatar'])
		@elseif($name == 'background_id')
			@include('form.image',['key'=>'background_id','id'=>$user->background_id,'image'=>$user->background,'type'=>'user_background'])
		@else
			<div class="form-group">
				<label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
				<div class="col-sm-8">
				  <input type="text" class="form-control" id="{{ $name }}" name="{{ $name }}" value="{{ $user->{$name} }}">
				</div>
			</div>
		@endif
	@endforeach
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div class="btn-group">
				<a class="btn btn-default" href="{{ Request::input('return') }}"><span class="glyphicon glyphicon-arrow-left"></span> Отмена</a>
				<button type="submit" class="btn btn-default" name="return" value="{{ Request::input('return') }}"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</button>
				@if($method == 'PUT')
				<button type="submit" class="btn btn-default" name="return" value="{{ URL::full() }}"><span class="glyphicon glyphicon-refresh"></span> Обновить</button>
				@endif
			</div>
		</div>
	</div>
	{!! csrf_field() !!}
</form>

@push('script')
<script>
    _token = '{{ csrf_token() }}';
</script>
<script src="{{ elixir('js/admin.js') }}" type="text/javascript"></script>
@endpush
