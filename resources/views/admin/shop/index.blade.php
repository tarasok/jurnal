@extends('layouts.master',[
    'metatitle'=>'Магазины',
])

@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1>Магазины <span class="badge">{{ $data->total() }}</span></h1>
            </div>
            <div class="col-md-2 btn-admin-group">
                <a class="btn btn-default btn-block" href="{{ route('admin.shops.create',array('return'=>URL::full())) }}"><span class="glyphicon glyphicon-plus"></span> Создать</a>
            </div>
        </div>
        <table class="table table-bordered table-hover table-striped">
		<tr>
            <th>id</th>
		    <th>Название</th>
		    <th>Пользователь</th>
		    <th>Активность</th>
            <th width="10"></th>
            <th width="10"></th>
            <th width="10"></th>
		</tr>
		@foreach($data as $model)
		<tr>
            <td>{{ $model->id }}</td>
		    <td>{{ $model->name }}</td>
            <td>@if($model->user){{ $model->user->name }}@endif</td>
            <td>@if(!$model->state)Не активен@endif</td>
            <td><a href="{{ route("admin.products.index",['shop_id'=>$model->id]) }}">Товары</a></td>
            <td><a href="{{ route('admin.shops.price',array('id'=>$model->id, 'return'=>URL::full())) }}">Прайс</a></td>
            <td><a href="{{ route('admin.shops.edit',array('id'=>$model->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
		</tr>
		@endforeach
	    </table>
        <div class="text-center">
            {{ $data->render() }}
        </div>
	</div>
@endsection

@push('style')
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush