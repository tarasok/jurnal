{!! Form::open(['url' => $action, 'class'=>'form-horizontal', 'method' => $method]) !!}
	@foreach($model->getFillable() as $name)
		@if($name == 'state')
            <div class="form-group">
                {!! Form::label($name, null, ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::select($name, [0 => 'Не опубликовано', 1 => 'Опубликовано'], $model->{$name}, ['class'=>'form-control']) !!}
                </div>
            </div>
        @elseif($name == 'user_id')
            <div class="form-group">
                {!! Form::label($name, null, ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::select($name, $users->prepend('Не выбрано', 0), $model->{$name}, ['class'=>'form-control']) !!}
                </div>
            </div>
		@else
			<div class="form-group">
                {!! Form::label($name, null, ['class'=>'col-sm-2 control-label']) !!}
				<div class="col-sm-8">
                    {!! Form::text($name, $model->{$name}, ['id' => $name,'class'=>'form-control']) !!}
				</div>
			</div>
		@endif
	@endforeach
    @include('form.buttons')
{!! Form::close() !!}
