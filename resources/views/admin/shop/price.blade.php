@extends('layouts.master',[
    'metatitle'=>'Магазины',
])

@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1>Прайс: {{ $shop->name }} <span class="badge">{{ $shop->price()->totalProducts() }}</span></h1>
            </div>
            <div class="col-md-5 btn-admin-group">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <a class="btn btn-default btn-block" href="{{ Request::input('return') }}"><span class="glyphicon glyphicon-arrow-left"></span> Отмена</a>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-default btn-block" href="{{ route('admin.shops.price.update',array('id'=>$shop->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-refresh"></span> Обновить</a>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-default btn-block" href="{{ route('admin.shops.price.import',array('id'=>$shop->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-import"></span> Импорт</a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-bordered table-hover table-striped">
		<tr>
            <th>id</th>
		    <th>Название</th>
		    <th>Цена</th>
		    <th width="100"></th>
		</tr>
		@foreach($shop->price()->products() as $product)
		<tr>
            <td>{{ $product->id }}</td>
            <td><a href="{{ $product->image }}" class="gallery-item">{{ $product->name }}</a></td>
            <td>{{ $product->price }}</td>
            <td><a href="{{ $product->url }}" target="_blank">В магазин</a></td>
		</tr>
		@endforeach
	    </table>
	</div>
@endsection

@push('style')
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush