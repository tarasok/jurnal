@extends('layouts.master',[
    'metatitle'=>'Создать магазин',
])

@section('content')
	<div class="container">
	    <h1>Создать магазин</h1>
		@include('admin.shop.form', [
		    			'action' => route('admin.shops.store'),
		    			'method' => 'POST',
		    		])
	</div>
@endsection

@push('style')
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush
