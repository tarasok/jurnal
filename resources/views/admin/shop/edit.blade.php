@extends('layouts.master',[
    'metatitle'=>'Редактировать магазин',
])

@section('content')
	<div class="container">
	    <h1>Редактировать магазин</h1>
		@include('admin.shop.form', [
		    			'action' => route('admin.shops.update',$model->id),
		    			'method' => 'PUT',
		    		])
	</div>
@endsection

@push('style')
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush

