@extends('layouts.master',[
    'metatitle'=>'Тэги',
])

@section('content')
	<div class="container">
	    <h1>Тэги</h1>
	    <table class="table table-bordered table-hover small">
		<tr>
		    <th>id</th>
		    <th>Назваие</th>
		    <th>Алиас</th>
		    <th>Статей</th>
		    <th></th>
		</tr>
		@foreach($tags as $tag)
		<tr>
            <td>{{ $tag->id }}</td>
            <td><a href="{{ $tag->url }}" target="_blank">{{ $tag->name }}</a></td>
		    <td>@if($tag->alias)<a href="{{ $tag->url }}" target="_blank">{{ $tag->alias }}</a>@endif</td>
		    <td align="right">{{ $tag->post_count }}</td>
            <td><a href="{{ route('admin.tags.show',array('id'=>$tag->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
		</tr>
		@endforeach
	    </table>
	</div>
@endsection