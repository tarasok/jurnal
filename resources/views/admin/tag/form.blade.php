@extends('layouts.master',[
    'metatitle'=>'Редактировать Тэг',
])

@section('content')
	<div class="container">
	    <h1>Редактировать Тэг "{{ $tag->name }}"</h1>
        <form class="form-horizontal" method="post" action="{{ route('admin.tags.update',$tag->id) }}">
            @foreach($tag->getFillable() as $name)
                @if($name == 'type')
                @else
                <div class="form-group">
                    <label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="{{ $name }}" name="{{ $name }}" value="{{ $tag->{$name} }}">
                    </div>
                </div>
                @endif
            @endforeach
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="btn-group">
						<a class="btn btn-default" href="{{ Request::input('return') }}"><span class="glyphicon glyphicon-arrow-left"></span> Отмена</a>
						<button type="submit" class="btn btn-default" name="return" value="{{ Request::input('return') }}"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</button>
						<button type="submit" class="btn btn-default" name="return" value="{{ URL::full() }}"><span class="glyphicon glyphicon-refresh"></span> Обновить</button>
					</div>
				</div>
			</div>
            {!! csrf_field() !!}
        </form>
	</div>
@endsection