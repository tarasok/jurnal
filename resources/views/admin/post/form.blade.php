@extends('layouts.master',[
    'metatitle'=>'Редактировать Пользователя',
])

@section('content')
	<div class="container">
		<br/>
		<br/>
        <form class="form-horizontal" method="POST" action="{{ route('admin.posts.update',$post->id) }}">
            <input name="_method" type="hidden" value="PUT">
            @foreach($post->getFillable() as $name)
                @if($name == 'text')
                    <div class="form-group">
                        <label for="{{ $name }}" class="col-sm-2 control-label" title="state">{{ $name }}</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="text" name="_text">{{ $post->text }}</textarea>
                        </div>
                    </div>
                    <script type="text/javascript">
                        CKEDITOR.replace( "text", {
                            toolbarGroups: [
                                {"name":"undo"},
                                {"name":"basicstyles","groups":["basicstyles"]},
                                {"name":"links","groups":["links"]},
                                {"name":"paragraph","groups":["list"]},
                                {"name":"styles","groups":["styles"]},
                                {"name":"document","groups":["mode"]},
                            ],
                            removeButtons: "Underline,Italic,Strike,Subscript,Superscript,Anchor",
                            extraPlugins: "autogrow",
                            format_tags: "p;h1;h2;h3;div;pre",
                            //allowedContent: true,
                            height: 100,
                            autoGrow_minHeight: 100,
                            autoGrow_bottomSpace: 20,
                            removePlugins: "resize,elementspath",
                        })
                    </script>
                @elseif($name == 'category_id')
                    <div class="form-group">
                        {!! Form::label($name, null, ['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::select($name, $categories->prepend('Не выбрано', 0), $post->{$name}, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                @elseif($name == 'state')
                    @include('form.select',['key'=>'state','value'=>$post->state,'options'=>['Не опубликовано'=>0,'Опубликовано'=>1]])
                @elseif($name == 'robots')
                    @include('form.select',['key'=>'robots','value'=>$post->robots,'options'=>['Индексировать'=>1,'Не индексировать'=>0]])
                @elseif($name == 'user_id')
                    @include('form.select',['key'=>'user_id','value'=>$post->user_id,'options'=>$users])
                @elseif($name == 'image_id')
                    @include('form.image',['key'=>'image_id','id'=>$post->image_id,'image'=>$post->image,'type'=>'post_image'])
                @elseif($name == 'header_id')
                    @include('form.image',['key'=>'header_id','id'=>$post->header_id,'image'=>$post->header,'type'=>'post_header'])
                @else
                    <div class="form-group">
                        {!! Form::label($name, null, ['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="{{ $name }}" name="{{ $name }}"
                                   value="{{ $post->{$name} }}">
                        </div>
                    </div>
                @endif
            @endforeach
            {{--<div class="form-group">--}}
                {{--<label class="col-sm-2 control-label" title="state">images</label>--}}
                {{--<div class="col-sm-8">--}}
                    {{--@foreach($post->images as $image)--}}
                        {{--<img src="{{ $image->url->thumb }}" />--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="btn-group">
						<a class="btn btn-default" href="{{ Request::input('return') }}"><span class="glyphicon glyphicon-arrow-left"></span> Отмена</a>
						<button type="submit" class="btn btn-default" name="return" value="{{ Request::input('return') }}"><span class="glyphicon glyphicon-floppy-disk"></span> Сохранить</button>
						<button type="submit" class="btn btn-default" name="return" value="{{ URL::full() }}"><span class="glyphicon glyphicon-refresh"></span> Обновить</button>
					</div>
				</div>
			</div>
            {!! csrf_field() !!}
        </form>
	</div>
@endsection

@push('style')
<link href="http://www.sceditor.com/minified/themes/default.min.css" rel="stylesheet" type="text/css" media="all"/>
<script src="http://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush

@push('script')
<script>
    _token = '{{ csrf_token() }}';
</script>
<script src="{{ elixir('js/admin.js') }}" type="text/javascript"></script>
@endpush
