@extends('layouts.master',[
    'metatitle'=>'Посты',
])

@section('content')
	<div class="container">
	    <h1>Посты <span class="badge">{{ $posts->total() }}</span></h1>
	    <table class="table table-bordered table-hover table-striped">
		<tr>
		    <th>id</th>
		    <th>Дата</th>
		    <th>Назваие / Алиас</th>
		    <th>Автор</th>
		    <th>Категория</th>
		    <th class="text-center"><span class="glyphicon glyphicon-eye-open"></span></th>
		    <th class="text-center"><span class="glyphicon glyphicon-star-empty"></span></th>
		    <th class="text-center"><span class="glyphicon glyphicon-thumbs-up"></span></th>
		    <th></th>
		</tr>
		@foreach($posts as $post)
		<tr>
            <td>{{ $post->id }}</td>
            <td>{{ $post->date }}</td>
            <td>
				<a href="{{ $post->url }}" target="_blank">{{ $post->title }}</a>
                <div class="small text-muted">
                    {{ $post->alias }}
                </div>
            </td>
		    <td width="200"><a href="{{ $post->user->url }}" target="_blank">{{ $post->user->name }}</a></td>
		    <td width="200">@if($post->category)<a href="{{ $post->category->url }}" target="_blank">{{ $post->category->title }}</a>@endif</td>
		    <td align="right">{{ $post->view_count }}</td>
		    <td align="right">{{ $post->view_avg }}</td>
		    <td align="right">{{ $post->share_count }}</td>
		    <td><a href="{{ route('admin.posts.edit',array('id'=>$post->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
		</tr>
		@endforeach
	    </table>
        <div class="text-center">
            {{ $posts->render() }}
        </div>
    </div>
@endsection

@push('style')
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush