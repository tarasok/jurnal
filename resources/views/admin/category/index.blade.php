@extends('layouts.master',[
    'metatitle'=>'Категории',
])

@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1>Категории <span class="badge">{{ $data->total() }}</span></h1>
            </div>
            <div class="col-md-2 btn-admin-group">
                <a class="btn btn-default btn-block" href="{{ route('admin.categories.create',array('return'=>URL::full())) }}"><span class="glyphicon glyphicon-plus"></span> Создать</a>
            </div>
        </div>
        <table class="table table-bordered table-hover table-striped">
		<tr>
		    <th>id</th>
		    <th>Название</th>
		    <th>Алиас</th>
		    <th>Родитель</th>
		    <th>Активность</th>
            <th width="10"></th>
		</tr>
		@foreach($data as $model)
		<tr>
            <td>{{ $model->id }}</td>
            <td><a href="{{ $model->url }}" target="_blank">{{ $model->title }}</a></td>
		    <td>{{ $model->alias }}</td>
            <td>@if($model->parent){{ $model->parent->title }}@endif</td>
            <td>@if(!$model->state)Не активна@endif</td>
			<td><a href="{{ route('admin.categories.edit',array('id'=>$model->id, 'return'=>URL::full())) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
		</tr>
		@endforeach
	    </table>
        <div class="text-center">
            {{ $data->render() }}
        </div>
	</div>
@endsection

@push('style')
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush