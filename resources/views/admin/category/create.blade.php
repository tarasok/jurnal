@extends('layouts.master',[
    'metatitle'=>'Создать категорию',
])

@section('content')
	<div class="container">
	    <h1>Создать категорию</h1>
		@include('admin.category.form', [
		    			'action' => route('admin.categories.store'),
		    			'method' => 'POST',
		    		])
	</div>
@endsection

@push('style')
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush
