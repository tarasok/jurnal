@extends('layouts.master',[
    'metatitle'=>'Редактировать категорию',
])

@section('content')
	<div class="container">
	    <h1>Редактировать категорию</h1>
		@include('admin.category.form', [
		    			'action' => route('admin.categories.update',$model->id),
		    			'method' => 'PUT',
		    		])
	</div>
@endsection

@push('style')
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush

