@extends('layouts.master',[
    'metatitle'=>'Магазины',
])

@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1>Товары <span class="badge">{{ $data->total() }}</span></h1>
            </div>
        </div>
        <table class="table table-bordered table-hover table-striped">
		<tr>
            <th width="70">id</th>
            <th width="100">Фото</th>
            <th>Название</th>
            <th width="150">Магазин</th>
            <th>Цена</th>
            <th width="100"></th>
		</tr>
		@foreach($data as $model)
		<tr>
            <td align="right">{{ $model->id }}</td>
            <td><a href="{{ $model->image }}" class="gallery-item"><img src="{{$model->image}}" width="100"></a></td>
            <td>{{ $model->name }}</td>
            <td>@if($model->shop){{ $model->shop->name }}@endif</td>
            <td align="right">{{ $model->price }}</td>
            <td><a href="{{ $model->url }}" target="_blank">В магазин</a></td>
        </tr>
		@endforeach
	    </table>
        <div class="text-center">
            {{ $data->render() }}
        </div>
	</div>
@endsection

@push('style')
<link href="{{ elixir('css/admin.css') }}" rel="stylesheet"/>
@endpush