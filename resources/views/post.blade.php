@extends('layouts.master', [
    'metatitle' => $post->title . ($post->photo_count >= 5 ? (' - ' .$post->photo_count.' фото'):''),
    'description' => $post->description,
    'url' => $post->url,
    'image_url' => $post->thumb,
    'author' => $post->user_alias ? $post->user_alias : $post->user->name,
    'robots' => $post->robots,
    'body' => 'data-type="Post" data-id='.$post->id.' data-shareable=true'
])

@section('content')
<div id="article">
    <div class="jumbotron jumbotron-title"{!! $post->header_url ? ' data-image="'. url($post->header_url).'"':'' !!}>
        <div class="container">
            <h1>{{ $post->title }}</h1>
            <ul class="list-inline">
				<li title="Дата публикации">{{ $post->date }}</li>
				<li title="Число просмотров"><span class="glyphicon glyphicon-eye-open"></span> {{ $post->view_count }}</li>
				<li title="Просмотры за день"><span class="glyphicon glyphicon-star-empty"></span> {{ $post->view_avg }}</li>
				<li title="Лайки"><span class="glyphicon glyphicon-thumbs-up"></span> {{ $post->share_count }}</li>
				<li title="Количество фотографий"><span class="glyphicon glyphicon-camera"></span> {{ $post->photo_count }}</li>
				<li title="Количество видео"><span class="glyphicon glyphicon-facetime-video"></span> {{ $post->video_count }}</li>
            </ul>
			<p><a href="{{ route('slug',$post->category->alias) }}" class="category-link"># {{ $post->category->title }}</a></p>
        </div>
    </div>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="content">
					{!! $post->text !!}
					@if($post->id >= 629)
						<p class="text-center">
							Если эта статья Вам показалась интересной и полезной - ставьте лайк и делитесь ссылкой с
							друзьями - это самый простой способ сказать "спасибо"!
						</p>
						<p class="text-center">
							<img alt="Поставь лайк" src="/images/blog_03/common/Like.gif">
						</p>
					@endif
				</div>
				@if($post->user->without_ad == false)
					<div class="banner-after">
						@include('ads.g2')
					</div>
				@endif
				@include('parts.social.shares')
				<p class="tags mb-30">
					Тэги: &nbsp;
					@foreach($post->tags as $tag)
						<a href="{{ $tag->url }}"><span class="label label-default">#{{ $tag->name }}</span></a>
					@endforeach
				</p>
			</div>
			<div class="col-md-4 visible-md visible-lg text-center mb-30">
				<div class="user">
					@if($post->user_alias)
						<p>Автор: {{ $post->user_alias }}</p>
					@else
						<p>
                            <a href="{{ $post->user->url }}">
                                @if($post->user->logo)
                                    <img class="avatar-image" src="{{ $post->user->logo }}" />
                                @else
                                    <span class="avatar-char avatar-char_color_{{ $post->user->id%10 }}">{{ str_limit($post->user->name,1,'') }}</span>
                                @endif
                            </a>
						</p>
                        <p>Автор: <a href="{{ $post->user->url }}">{{ $post->user->name }}</a></p>
						@if($post->user->title)
							<p class="description small"><b>{{ $post->user->title }}</b></p>
						@endif
                        <p class="text-muted small"><a href="{{ route('users') }}">Рейтинг</a>: {{ $post->user->rating }}</p>
					@endif
				</div>
			</div>
			@if($post->user->without_ad == false)
				<div class="col-md-4 visible-md visible-lg text-center mb-30">
					@include('ads.g3')
				</div>
			@endif
			<div class="col-md-4 visible-md visible-lg">
				@include('parts.social.group')
			</div>
		</div>
	</div>
    <div class="separator"></div>
    @include('parts.related')
</div>
@endsection

@push('script')
<script>
	$.post("{{ action('CountController@view')}}","id={{$post->id}}&type=Post&_token={{ csrf_token() }}");
	$( ".ya-share2" ).on( "click", ".ya-share2__link", function() {
		$.post("{{ action('CountController@share')}}","id={{$post->id}}&type=Post&_token={{ csrf_token() }}");
	});
</script>
@endpush