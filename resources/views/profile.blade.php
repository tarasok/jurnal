@extends('layouts.master', [
    'metatitle' => 'Мой профиль',
])

@section('content')
	<div class="container">
	    <h1>Мой профиль</h1>
        <form class="form-horizontal" method="post" action="">

            <div class="form-group">
                <label for="" class="col-sm-2 control-label" >Аватар</label>
                <div class="col-sm-8">
                    <button class="btn btn-default btn-image-upload" data-target="#profile-avatar" data-type="user_avatar"><span class="glyphicon glyphicon-plus"></span> Добавить</button>
                    <span id="profile-avatar">No photo</span>
                </div>
            </div>

            {!! csrf_field() !!}
        </form>
    </div>
@endsection

@push('script')

    <script>
        _token = '{{ csrf_token() }}';
    </script>

    <script>
        $(document).on('click', '.btn-image-upload', function(e) {
            $('#form-upload').remove();
            $('body').prepend(
                '<form enctype="multipart/form-data" id="form-upload" style="display: none;">' +
                '   <input type="file" name="upload-image" value="" data-target="'+$(this).data('target')+'" />' +
                '   <input type="hidden" name="type" value="'+$(this).data('type')+'" />' +
                '</form>'
            );
            $('#form-upload input').trigger('click');
            e.preventDefault();
        });


        $(document).delegate(':file', 'change', function () {

            var target = $(this).data('target');

            $.ajax({
                url: '/api/image?_token=' + _token,
                type: 'post',
                dataType: 'json',
                data: new FormData($('#form-upload')[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
//                    $('#image-upload i').addClass('fa-circle-o-notch fa-spin');
                },
                complete: function () {
//                    $('#image-upload i').removeClass('fa-circle-o-notch fa-spin');
                },
                success: function (json) {

                    //alert(json.full);
                    $(target).html('<img src="'+json.url_thumb+'" alt="" />');

//                    if (json['error']) {
//                        alert(json['error']);
//                    }
//                    if (json['success']) {
//                        var image = {image: json.image, url: json.src, is_main: false};
//                        $('#images li:last').before(viewImage(image));
//                        renderImages();
//                    }
                },
                error: function (xhr) {
                    alert(xhr.statusText);
                }
            });
        });


    </script>
@endpush

@push('style')
    <style>

    </style>
@endpush