@extends('layouts.master', [
    'metatitle' => 'Тэг: '.$tag->name,
    'robots' => $tag->alias?true:false
])

@section('content')
    <div id="articles">
	<div class="container title text-center">
	    <h1>Тэг: {{ $tag->name }}</h1>
	</div>
	@include('parts.posts')
    </div>
@endsection