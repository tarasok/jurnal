@extends('layouts.master',[
    'metatitle'=>'Рейтинг авторов',
])

@section('content')
    <div id="users">
        <div class="container">
            <h1 class="text-center mb-30">
                Рейтинг авторов
            </h1>
            <div class="row">
                @foreach($users as $i=>$user)
                    <div class="col-xs-6 col-sm-4 col-md-2">
                        <div class="user text-center mb-30">
                            <a href="{{ $user->url }}">
                            @if($user->logo)
                                <img class="avatar-image avatar-image_size_middle mb-10" src="{{ $user->logo }}"/>
                            @else
                                <span class="avatar-char avatar-char_size_middle avatar-char_color_{{ $user->id%10 }} mb-20">{{ str_limit($user->name,1,'') }}</span>
                            @endif
                            </a>
                            <p><a href="{{ $user->url }}">{{ $user->name }}</a></p>
                            <p class="small">Рейтинг: {{ $user->rating }}</p>
                        </div>
                    </div>
                    @if (($i+1)%6==0)
                        <div class="clearfix visible-md visible-lg"></div>
                    @endif
                    @if (($i+1)%3==0)
                        <div class="clearfix visible-sm"></div>
                    @endif
                    @if (($i+1)%2==0)
                        <div class="clearfix visible-xs"></div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection