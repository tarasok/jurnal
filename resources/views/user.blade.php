@extends('layouts.master',[
    'metatitle'=>$user->name . ($user->title ? ' - '. $user->title : '').' в Мебельном журнале',
    'description' => $user->description,
    'url' => $user->url,
    'image_url' => $user->logo,
    'author' => $user->name,
    'robots' => $user->alias?true:false
])

@section('content')
	<div id="user" style="background-image: url('{{ $user->background_old }}');">
		<div class="container">
			@if($user->logo)
				<img class="avatar-image avatar-image_size_big" src="{{ $user->logo }}" />
			@else
				<span class="avatar-char avatar-char_size_middle avatar-char_color_{{ $user->id%10 }}">{{ str_limit($user->name,1,'') }}</span>
			@endif
            <h1>{{ $user->name }}</h1>
            @if($user->title)
                <p><b>{{ $user->title }}</b></p>
            @endif
			@if($user->site)
                <p class="site"><i><a href="{{ $user->site }}" target="_blank">{{ str_replace(['http://','https://'],'',$user->site) }}</a></i></p>
            @endif
            <ul class="list-inline">
				<li title="Рейтинг"><span class="glyphicon glyphicon-king"></span> {{ $user->rating }}</li>
				<li title="Число просмотров"><span class="glyphicon glyphicon-eye-open"></span> {{ $user->post_view_count }}</li>
				<li title="Просмотры за день"><span class="glyphicon glyphicon-star-empty"></span> {{ $user->view_avg }}</li>
				<li title="Понравилось"><span class="glyphicon glyphicon-thumbs-up"></span> {{ $user->share_count }}</li>
				<li title="Всех фото"><span class="glyphicon glyphicon-camera"></span> {{ $user->photo_count }}</li>
				<li title="Всех видео"><span class="glyphicon glyphicon-facetime-video"></span> {{ $user->video_count }}</li>
				<li title="Всех статей"><span class="glyphicon glyphicon-pencil"></span> {{ $user->post_count }}</li>
			</ul>
            @if($user->description)
                <p class="description">{{ $user->description }}</p>
            @endif
		</div>
	</div>
	<div id="articles">
		@include('parts.posts')
	</div>
@endsection

@push('script')
<script>
	$.post("{{ action('CountController@view')}}","id={{$user->id}}&type=User&_token={{ csrf_token() }}");
</script>
@endpush