@extends('layouts.master', [
    'url' => url('/'),
    'robots' => true
])

@section('content')
    <div id="articles">
	@include('parts.lead')
	@include('parts.best')
	@for ($i=0; $i<=8; $i++) <?php unset($posts[$i]) ?> @endfor
	<div class="container title">
	    <h1>Полезные статьи</h1>
	</div>
	@include('parts.posts')
    </div>
@endsection