/**
 * Created by taras on 06.06.17.
 */
$(document).on('click', '.btn-image-upload', function (e) {
    $('#form-upload').remove();
    $('body').prepend(
        '<form enctype="multipart/form-data" id="form-upload" style="display: none;">' +
        '   <input type="file" name="upload-image" value="" data-key="' + $(this).data('key') + '" data-target="' + $(this).data('target') + '" />' +
        '   <input type="hidden" name="type" value="' + $(this).data('type') + '" />' +
        '   <input type="hidden" name="_token" value="' + _token + '" />' +
        '</form>'
    );
    $('#form-upload input').trigger('click');
    e.preventDefault();
});

$(document).on('click', '.btn-image-upload-delete', function (e) {
    $(this).closest('span').find('input').val(0);
    $(this).closest('span').find('button, img').remove();
    e.preventDefault();
});

$(document).delegate(':file', 'change', function () {

    var target = $(this).data('target');
    var key = $(this).data('key');

    $.ajax({
        url: '/api/image',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            // $('#image-upload i').addClass('fa-circle-o-notch fa-spin');
        },
        complete: function () {
            // $('#image-upload i').removeClass('fa-circle-o-notch fa-spin');
        },
        success: function (json) {
            $(target).html(
                '<button class="btn btn-default btn-image-upload-delete pull-left"><span class="glyphicon glyphicon-trash"></span> Удалить</button>' +
                '<img src="' + json.url_thumb + '" alt="" />' +
                '<input type="hidden" name="' + key + '" value="' + json.id + '" />'
            );
        },
        error: function (xhr) {
            alert(xhr.statusText);
        }
    });
});