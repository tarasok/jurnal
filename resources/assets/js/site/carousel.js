// Carousel
$(document).ready(function() {
	var owl = $(".owl-carousel");
	owl.owlCarousel({
		loop:true,
		responsiveClass:true,
		itemsCustom: [[0, 1], [600, 2], [992, 3]],
		//scrollPerPage: true,
		//navigation: true,
		navigationText: ['<span class="glyphicon glyphicon-chevron-left"></span>','<span class="glyphicon glyphicon-chevron-right"></span>'],

	});
	$(".owl-next").click(function(){
		owl.trigger('owl.next');
		return false;
	})
	$(".owl-prev").click(function(){
		owl.trigger('owl.prev');
		return false;
	})
});