// Lightbox gallery
jQuery(document).ready(function() {
	$('.gallery-item').magnificPopup({
		type: 'image',
		gallery:{
			enabled:true
		},
		image: {
			titleSrc: function(item) {
				return item.el.find("img").attr('title');
			}
		}
	});
});
