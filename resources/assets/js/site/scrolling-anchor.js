// Smooth scrolling anchor
jQuery(document).ready(function() {
    $(document).on('click', 'a[href^="#"]:not(.not-scroll)', function () {
	var hash = this.hash.slice(1);
	if(hash && $('#'+hash).length){
	    $('html, body').animate({ scrollTop:  $('#'+hash).offset().top }, 800 );
	    //yaCounter33317708.params({hash:hash});
	    //yaCounter33317708.hit('#'+hash);
	}
	return false;
    });
});