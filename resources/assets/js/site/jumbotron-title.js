// title
jQuery(document).ready(function() {
    var image = $('.jumbotron-title').data('image');
    if(image){

		$('.jumbotron-title')
			.prepend('<div class="blackout"></div>')
			.prepend('<div class="cover"></div>')

		var img = new Image();
		$(img).attr('src', image).on('load',function() {

			var blur = '';
			var width = img.naturalWidth;
			if(width<768){
				blur = 'blur-xs';
			}else if(width<992){
				blur = 'blur-sm';
			}else if(width<1200){
				blur = 'blur-md';
			}else{
				blur = 'blur-lg';
			}

			$('.jumbotron-title .cover')
			.css('background-image','url('+image+')')
			.addClass(blur)
			.css('opacity','1');

			$('.jumbotron-title').addClass('image');
		});
    }
});