// Переход по ссылке с подстверждением

$(document).ready(function() {
	$(".link-confirm").click(function( e ) {
		e.preventDefault();
		var msg = 'Вы уверены?';
		if($(this).attr('title')){
			msg = $(this).attr('title')+'?';
		}
		if($(this).data('msg')){
			msg = $(this).data('msg');
		}
		if (confirm(msg)) {
			window.location.href = $(this).attr('href');
		}
	})
});