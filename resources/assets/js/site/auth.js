$('document').ready(function(){
    $('#form-auth').submit(function (ev) {
	var form = $(this);
	var btn = $(this).find('button');
	$.ajax({
	    type: 'post',
	    url: form.attr('action'),
	    data: form.serialize(),
	    dataType: 'json',
	    beforeSend: function () {
		btn.button('loading')
		form.find('.alert').remove();
		form.find('.help-block').remove();
		form.find('.has-error').removeClass('has-error');
	    },
	    complete: function (jqXHR) {
		btn.button('reset');
		if(jqXHR.responseJSON.redirect !== undefined){
		   document.location.href = jqXHR.responseJSON.redirect;
		}
	    },
	    success: function (json) {
		form.append('<div class="alert alert-success">'+json.message+'</div>');
	    },
	    error: function (jqXHR) {
		var message;
		//console.log(jqXHR);
		switch (jqXHR.status) {
		    case 404:
			message = jqXHR.responseJSON.message;
			break;
		    case 422:
			if(jqXHR.responseJSON['token'] !== undefined){
			    message = "Неверный токен!";
			}else{
			    for(var name in jqXHR.responseJSON) {
				form.find('[name='+name+']').after('<span class="help-block">'+jqXHR.responseJSON[name][0]+'</span>')
				form.find('[name='+name+']').parent().addClass('has-error');
			    }
			}
			break;
		    default:
			message = jqXHR.statusText;
		}
		if(message){
		    form.append('<div class="alert alert-danger">'+message+'</div>');
		}
	    }
	});
	ev.preventDefault();
    });
});
