<?php
namespace Deployer;

// Подключим основные рецепты из Deployer'а
require 'recipe/common.php';
require 'recipe/laravel.php';
//require 'recipe/laravel.php';

/*
 * Installation:
 * 
 * curl -LO https://deployer.org/deployer.phar
 * mv deployer.phar /usr/local/bin/dep
 * chmod +x /usr/local/bin/dep
 * 
 */

// Configuration

 set('ssh_type', 'native');
 set('ssh_multiplexing', true);
// /var/www/deploy/data/www/jurnal
set('repository', 'git@bitbucket.org:tarasok/jurnal.git');
set('default_stage', 'develop');

host('testing.mebeljurnal.ru')
    ->stage('develop')
    ->user('developer')
    ->set('deploy_path', '/home/developer/testing.mebeljurnal.ru');

task('deploy', [
    'deploy:down',
    'dump',
    'git',
    'deploy:composer',
    //'deploy:dump-autoload',
    'deploy:migrate',
    'deploy:npm',
    'deploy:gulp',
    'deploy:clean-cached-data',
    'deploy:create-route-cache',
    'deploy:create-config-cache',
    'deploy:up',
]);

task('deploy:down', function() {
    write(run('cd {{deploy_path}} && php artisan down'));
});

task('deploy:up', function() {
    write(run('cd {{deploy_path}} && php artisan up'));
});

task('git', function() {
    write(run('cd {{deploy_path}} && git pull'));
});

task('dump', function() {
    write(run('cd {{deploy_path}} && php artisan db:dump'));
});

task('deploy:migrate', function() {
    write(run('cd {{deploy_path}} && php artisan migrate'));
});

task('deploy:composer', function() {
    run('cd {{deploy_path}} && composer install');
});

task('deploy:dump-autoload', function() {
    run('cd {{deploy_path}} && composer dump-autoload');
});

task('deploy:npm', function() {
    run('cd {{deploy_path}} && npm update');
});

task('deploy:gulp', function() {
    run('cd {{deploy_path}} && gulp --production');
});

task('deploy:create-route-cache', function() {
    run('cd {{deploy_path}} && php artisan route:cache');
});

task('deploy:create-config-cache', function() {
    run('cd {{deploy_path}} && php artisan config:cache');
});

task('deploy:clean-cached-data', function() {
    run('cd {{deploy_path}} && php artisan cache:clear');
});

task('sync:posts', function() {
    run('cd {{deploy_path}} && php artisan sync:posts');
});

task('sync:users', function() {
    run('cd {{deploy_path}} && php artisan sync:users');
});

task('sync:categories', function() {
    run('cd {{deploy_path}} && php artisan sync:categories');
});

task('sync:urls', function() {
    run('cd {{deploy_path}} && php artisan sync:urls');
});