# mebeljurnal.ru

- development: [dev.mebeljurnal.ru](http://dev.mebeljurnal.ru)
- product: [mebeljurnal.ru](http://mebeljurnal.ru)

## Installation
1. Clone the project to preferred directory

    ```
    $ git clone git@bitbucket.org:tarasok/jurnal.git jurnal
    ```

2. Install dependencies

    ```
    $ cd jurnal && composer install
    ```

3. Fill config file

    ```
    $ cp .env.example .env
    ```
    
    ```
    $ php artisan key:generate
    ```

4. Install node moduless

    ```
    $ npm install
    ```

### DataBase restore

DataBase restore from database/last.sql
```
php artisan db:restore
```

### Creating Symbolic Links

Create a symbolic link from "public/upload" to OLD_IMAGES_PATH (set in .env file)
```
php artisan filesystem:images-link
```
Create a symbolic link from "public/upload" to "storage/app/public"
```
php artisan filesystem:upload-link
```