var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix
		.styles('site','public/css/site.css')
		.styles('admin','public/css/admin.css')

		.scripts('site','public/js/site.js')
		.scripts('admin','public/js/admin.js')
		.scripts([
			'jquery/dist/jquery.js',
			'bootstrap/dist/js/bootstrap.js',
			'owlcarousel/owl-carousel/owl.carousel.js',
			'magnific-popup/dist/jquery.magnific-popup.js',
			'vue/dist/vue.js'
		],'public/js/vendors.js','node_modules')
		
		.copy('node_modules/jquery/dist/jquery.min.js', 'public/vendors/jquery')

        .copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/vendors/bootstrap/css/')
        .copy('node_modules/bootstrap/dist/css/bootstrap.min.css.map', 'public/vendors/bootstrap/css/')
		.copy('node_modules/bootstrap/dist/fonts/*.*', 'public/vendors/bootstrap/fonts')

        .copy('node_modules/owlcarousel/owl-carousel/owl.carousel.css', 'public/vendors/owlcarousel')
        .copy('node_modules/owlcarousel/owl-carousel/grabbing.png', 'public/vendors/owlcarousel')

        .copy('node_modules/magnific-popup/dist/magnific-popup.css', 'public/vendors/magnific-popup')

		.version(['css/site.css', 'css/admin.css', 'js/site.js', 'js/admin.js', 'js/vendors.js']);

});
