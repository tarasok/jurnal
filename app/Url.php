<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable = ['url','status','type','type_id','referer'];
    
    public function getRedirectAttribute()
    {
        if($this->type and $this->type_id){
            $model = '\App\\'.ucfirst($this->type);
            if($model){
                return $model::find($this->type_id)->url;
            }
        }
    }
}
