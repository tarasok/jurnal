<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Cache;

class SlugController extends Controller
{
	public function index($slug)
	{
		$union1 = Cache::remember('slug.'.$slug, 60, function() use ($slug) {
			
			$union2 = DB::table('posts')->select('id','alias')
				->addSelect(DB::raw("'post' as `type`"))
				->where('alias',$slug);
			
			$union3 = DB::table('users')->select('id','alias')
				->addSelect(DB::raw("'user' as `type`"))
				->where('alias',$slug);
			
			$union4 = DB::table('tags')->select('id','alias')
				->addSelect(DB::raw("'tag' as `type`"))
				->where('alias',$slug);

			$union1 = DB::table('categories')->select('id','alias')
				->addSelect(DB::raw("'category' as `type`"))
				->where('alias',$slug)

				->union($union2)
				->union($union3)
				->union($union4)
				->first();
			
			return $union1;
		});
		
		switch ($union1->type ?? null){
			case 'post':
				return \App::call('\App\Http\Controllers\PostController@show',[$union1->id]);	
			case 'category':
				return \App::call('\App\Http\Controllers\PostsController@category',[$union1->id]);
			case 'user':
				return \App::call('\App\Http\Controllers\PostsController@user',[$union1->id]);
			case 'tag':
				return \App::call('\App\Http\Controllers\PostsController@tag',[$union1->id]);
			default:
				abort(404);
		}
	}
}
