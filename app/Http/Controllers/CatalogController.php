<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CatalogController extends Controller
{
    public function index($ids){
		$ids = explode('-', $ids);
		$items = \App\Item::withImage()->option($ids)->best()->take(48)->get();
		$options_menu = \App\Option::where('level',0)->get();
		$options = \App\Option::whereIn('id',$ids)->pluck('title','id')->toArray();
		$current_id = key($options);
		$title = ucfirst(implode(', ', $options));
		return view('items',compact('items','title','options','current_id','options_menu'));
	}
}
