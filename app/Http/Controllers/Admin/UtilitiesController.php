<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Response;

class UtilitiesController extends Controller
{
    public function sorry()
    {
		return Response::view('errors.sorry', [], 503);
    }

    public function phpinfo()
    {
		phpinfo();
    }
}
