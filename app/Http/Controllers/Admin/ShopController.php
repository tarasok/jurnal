<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Shop;
use App\User;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Shop::with('user')->orderBy('id','desc')->paginate(30);
        return view('admin.shop.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Shop;
        $users = User::orderBy('id','desc')->pluck('name', 'id');
        return view('admin.shop.create',compact('model','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = Shop::create($request->all());
        return redirect($request->input('return'))->with('message', 'Категория <b>'.$model->title.'</b> сохранена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Shop::findOrFail($id);
        $users = User::orderBy('id','desc')->pluck('name', 'id');
        return view('admin.shop.edit',compact('model','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Shop::findOrFail($id);
        $model->update($request->all());
        return redirect($request->input('return'))->with('message', 'Категория <b>'.$model->title.'</b> сохранена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function priceIndex($id)
    {
        $shop = Shop::findOrFail($id);
        return view('admin.shop.price',compact('shop'));
    }

    public function priceUpdate(Request $request, $id)
    {
        Shop::findOrFail($id)->price()->load();
        return redirect($request->input('return'))->with('message', 'Прайс скопирован на сервер. Теперь его можно импортировать');
    }

    public function priceImport(Request $request, $id)
    {
        $count = Shop::findOrFail($id)->price()->import();
        return redirect($request->input('return'))->with('message', 'Импортированно <b>'.$count.'</b> товаров');
    }
}
