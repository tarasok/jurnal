<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Artisan;

class SyncController extends Controller
{
    public function index($subject,$id)
    {
		Artisan::call('sync:'.$subject, ['id' => $id]);
		return 'sync.'.$subject.'.'.$id;
    }
}
