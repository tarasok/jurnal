<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Img;

use App\Http\Requests;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($file = $request->file('upload-image') and $type = $request->input('type')) {

            $upload_path = storage_path('app/upload/');
            $path = '';

            if (!file_exists($upload_path . $type)) {
                mkdir($upload_path . $type, 0777, true);
            }

            $segments = [rand(10, 20), rand(10, 20), rand(10, 20)];

            foreach ($segments as $i=>$segment) {
                if($i!=0){
                    $path .= '/';
                }
                $path .= $segment;
                if (!file_exists($upload_path . $type . '/' . $path)) {
                    mkdir($upload_path . $type . '/' . $path, 0777, true);
                }
            }

            $image = new Image;
            $image->type = $type;
            $image->path = $path;
            $image->name = md5(uniqid());
            $image->user_id = $request->user()->id;
            $image->extention = strtolower($file->getClientOriginalExtension());
            $versions = [];

            foreach ($image->versions_config as $version_name => $vc) {
                $img = Img::make($file);
                if ($vc['method'] == 'fit') {
                    $img->fit($vc['width'], $vc['height']);
                }
                if ($vc['method'] == 'resize') {
                    $img->resize($vc['width'], $vc['height'], function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                }
                $img->save($upload_path . $image->filepath($version_name), $vc['quality']??null);

                $versions[] = $version_name;
            }

            $image->versions = $versions;
            $image->save();
            return $image;
        } else {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
