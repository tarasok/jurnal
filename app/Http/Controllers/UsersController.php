<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::Where('post_count','>',' 0')->orderBy('rating','desc')->get();
        return view('users',compact('users'));
    }
}
