<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use App\Category;

class XmlController extends Controller
{
    public function sitemap()
	{
		$links = [];
		
		Post::whereIn('category_id',Category::ids(78))
			->published()
			->where('robots',1)
			->orderBy('id','DESC')
			->get()
			->each(function ($post) use (&$links) {
				$links[] = $post->url;
			});
			
		Category::whereIn('id',Category::ids(78))
			->where('id','<>',78)
			->where('state',1)
			->orderBy('order')
			->get()
			->each(function ($post) use (&$links) {
				$links[] = $post->url;
			});
		
		return response()
            ->view('xml.sitemap', compact('links'), 200)
            ->header('Content-Type', 'text/xm');
	}
}
