<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Cache;

class PostController extends Controller
{
    public function show($id)
    {
		$post = \App\Post::with('user','header')->findOrFail($id);
		if($post->state != 1 or $post->category->state != 1){
			abort(404);
		}
		$post->text = $this->replaceCatalog($post->text);
		$post->text = $this->replaceYoutube($post->text);
		return view('post',compact('post'));
    }
	
	private function replaceCatalog($text)
	{
		$regex		= '/<p>{modena\s+(.*?)}<\/p>/i';
		preg_match_all($regex, $text, $matches, PREG_SET_ORDER);
		if ($matches) {
			foreach ($matches as $match) {
				$matcheslist =  explode(',', $match[1]);
				$select = trim($matcheslist[0]);
				$title = trim($matcheslist[1]);
				parse_str($select, $values);
				$ids = explode('-', $values['select'] ?? '');
				$key = 'items.post.'.$values['select'] ?? '';
				$items = Cache::remember($key, 30, function() use ($ids) {
					 return \App\Item::withImage()->option($ids)->best()->take(4)->get();
				});
				
				if($items){
					$output = view('parts.items',compact('items','title','select'));
					$text = preg_replace("|$match[0]|", addcslashes($output, '\\'), $text, 1);
				}
			}
		}
		return $text;
	}
	
	private function replaceYoutube($text)
	{
		$regex = '/<p(.*)>{youtube(\s|&nbsp;)(.*?)}<\/p>/i';
		preg_match_all($regex, $text, $matches, PREG_SET_ORDER);
		if ($matches) {
			foreach ($matches as $match) {
				$video_id = $match[3];
				$output = view('parts.youtube',compact('video_id'));
				$text = preg_replace("|$match[0]|", addcslashes($output, '\\'), $text, 1);
			}
		}
		return $text;
	}
}
