<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Cache;

class CountController extends Controller
{
    public function view(Request $request)
    {
        $id = $request->input('id');
		$model = '\App\\'.$request->input('type');
		$subject = $model::findOrFail($id);
		$subject->view_count = $subject->view_count + 1;
		$subject->save(); // for observer
		return $subject->view_count;
	}
	
    public function share(Request $request)
    {
        $id = $request->input('id');
		$model = '\App\\'.$request->input('type');
		$subject = $model::findOrFail($id);
		$subject->share_count = $subject->share_count + 1;
		$subject->save(); // for observer
		Cache::flush();
		return $subject->share_count;
	}
}
