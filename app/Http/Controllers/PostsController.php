<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use Cache;

class PostsController extends Controller
{
	public function index()
	{
		$posts = Cache::remember('index', 15, function() {
			return Post::published()->where('category_id','<>', 100)->take(24)->orderBy('id','DESC')->paginate(24);
		});

		$next_page_url = route('api.post',['page'=>2]);
		return view('index',compact('posts','next_page_url'));
	}
	
    public function category($id)
    {
			$category = \App\Category::findOrFail($id);
					
			if($category->state != 1){
				abort(404);
			}
        
			$posts = Post::whereIn('category_id',\App\Category::ids($id))->published()->orderBy('id','DESC')->paginate(24);

			$next_page_url = route('api.post',['page'=>2,'category_id'=>$id]);

			return view('category',compact('posts','category','next_page_url'));
    }
	
    public function user($id)
    {
		$user = \App\User::findOrFail($id);
		$posts = $user->posts()->published()->where('user_alias','')->orderBy('id','DESC')->paginate(24);
		$next_page_url = route('api.post',['page'=>2,'user_id'=>$id]);
		return view('user',compact('posts','user','next_page_url'));
    }
	
    public function tag($id)
    {
		$tag = \App\Tag::findOrFail($id);
		$posts = $tag->posts()->published()->orderBy('id','DESC')->paginate(24);

		$next_page_url = route('api.post',['page'=>2,'tag_id'=>$id]);

		return view('tag',compact('posts','tag','next_page_url'));
    }
	
    public function search(Request $request)
    {
		$q = $request->input('q');
		$posts = Post::search($request->input('q'))->published()->orderBy('id','DESC')->paginate(24);
		$next_page_url = route('api.post',['page'=>2,'q'=>$q]);
		return view('search',compact('posts','next_page_url','q'));
    }
    public function api(Request $request)
    {
		$key =	$request->path().'?'.
			'category_id'.'='.$request->input('category_id').'&'.
			'user_id'.'='.$request->input('user_id').'&'.
			'tag_id'.'='.$request->input('tag_id').'&'.
			'q'.'='.$request->input('q').'&'.
			'per_page'.'='.$request->input('per_page',24).'&'.
			'page'.'='.$request->input('page');

		return Cache::remember($key, 15, function() use ($request) {

			$query = Post::published()->orderBy('id','DESC');

			if($request->input('user_id')){
				$query = $query->where('user_id',$request->input('user_id'))->where('user_alias','');
			}

			if($request->input('tag_id')){
				$query = $query->whereHas('tags', function ($query) use ($request) {
					$query->where('id', $request->input('tag_id'));
				});
			}

			if($request->input('q')){
				$query = $query->search($request->input('q'));
			}
			
			if($request->input('category_id')){
				$query = $query->whereIn('category_id',\App\Category::ids($request->input('category_id')));
			}

			return $query->paginate($request->input('per_page',24))->appends([
				'category_id' => $request->input('category_id'),
				'user_id' => $request->input('user_id'),
				'q' => $request->input('q'),
				'per_page' => $request->input('per_page',24),
			])->toJson(JSON_UNESCAPED_UNICODE);
		});
    }
    
    public function all()
    {
		return Post::published()->get()->makeVisible(['description','created_at']);
    }
}
