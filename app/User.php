<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Slugable;

    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'alias',
        'avatar_id',
        'background_id',
        'title',
        'description',
        'site',
        'without_ad'
    ];
    protected $visible = ['id', 'name'];

    public function posts()
    {
        return $this->hasMany('App\Post', 'user_id');
    }

    public function avatar()
    {
        return $this->hasOne('App\Image', 'id', 'avatar_id');
    }

    public function background()
    {
        return $this->hasOne('App\Image', 'id', 'background_id');
    }

    public function setAvatarIdAttribute($value)
    {
        Image::toggleIsRelated($this->attributes['avatar_id'] ?? 0, $value);
        $this->attributes['avatar_id'] = $value;
    }

    public function setBackgroundIdAttribute($value)
    {
        Image::toggleIsRelated($this->attributes['background_id'] ?? 0, $value);
        $this->attributes['background_id'] = $value;
    }

    public function getLogoAttribute()
    {
        if ($this->avatar) {
            return $this->avatar->url->full;
        }
    }

    public function getBackgroundOldAttribute()
    {
        if ($this->background) {
            return $this->background->url->full;
        }else{
            return url('images/users/default-background.jpg');
        }
    }
}
