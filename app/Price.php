<?php

namespace App;
use Storage;

class Price
{
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
        $this->file = 'price/' . $this->shop->id . '.yml';
        if (!Storage::exists($this->file)) {
            $this->load();
        }
        $this->yml = simplexml_load_string(Storage::get($this->file));
    }

    public function load()
    {
        Storage::put($this->file, file_get_contents($this->shop->url_yml));
    }

    public function import()
    {
        Product::where('shop_id', $this->shop->id)->delete();
        foreach($this->products() as $productFromPrice){
            $product = new Product;
            $product->name = $productFromPrice->name;
            $product->image = $productFromPrice->image;
            $product->price = $productFromPrice->price;
            $product->url = $productFromPrice->url;
            $product->shop_id = $this->shop->id;
            $product->save();
        }

        return $this->totalProducts();
    }

    public function products()
    {
        //print_r($this->yml->shop->offers->offer[0]);

        if(!isset($this->_products)){
            $prducts = [];
            foreach($this->yml->shop->offers->offer as $offer){
                $prducts[] = (object)[
                    'id'=> (string)$offer->attributes()->id,
                    'name'=> (string)$offer->name,
                    'image'=> (string)$offer->picture,
                    'price'=> (string)$offer->price,
                    'url'=> (string)$offer->url,
                ];
            }
            $this->_products = $prducts;
        }
        return $this->_products;
    }
    public function totalProducts()
    {
        return count($this->products());
    }
}
