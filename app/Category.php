<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Slugable;
use Cache;

class Category extends Model
{
    use Slugable;
    
    protected $table = 'categories';
    protected $appends = ['url'];
    protected $visible = ['id','title','url'];
    protected $fillable = [
        'title',
        'alias',
        'title2',
        'text',
        'description',
        'image_full',
        'state',
        'parent_id',
        'order',
    ];
    
    public function posts()
    {
        return $this->hasMany('App\Post','category_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category');
    }
	
    public static function ids($category_id)
    {
		return Cache::remember('ids.category.'.$category_id, 60, function() use ($category_id) {
			
			$ids = [$category_id];
			
			for ($i=0; $i < count($ids); $i++){
				$child_ids = Self::where('parent_id',$ids[$i])->pluck('id')->toArray();
				$ids = array_merge($ids, $child_ids);
			}
			
			return $ids;
		});
    }
    
    public function getMetaTitleAttribute()
    {
        if($this->title2){
            return str_replace(['::','br'], '-', $this->title2);
        }else{
            return $this->title;
        }
    }
    
    public function getHeaderTitleAttribute()
    {
        return '<big>'.str_replace(['::','br'], ['</big>','<br/>'], $this->title2);
    }
}
