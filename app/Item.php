<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'nios_my_items';
    public $timestamps = false;
    protected $connection = 'palmira';
    protected $visible = ['title','thumb','url'];
	protected $appends = ['url','thumb'];
	
	public function options()
	{
	  return $this->belongsToMany('App\Option', 'nios_my_tags_xref', 'item_id', 'tag_id');
	}
	
    public function getUrlAttribute()
    {
        return 'http://palmiramebel.ru/item/'.$this->id.'.html';
    }
	
    public function getThumbAttribute()
    {
        return 'http://palmiramebel.ru/images/items/resized/'.str_replace('.', '_crop_251x189.', $this->image);
    }
	
    public function scopeWithImage($query)
    {
        return $query
			->addSelect('nios_my_items.*','nios_my_items_images.image as image')
			->leftJoin('nios_my_items_images', 'nios_my_items.image_id', '=', 'nios_my_items_images.image_id');
    }
	
    public function scopeBest($query)
    {
        return $query->whereState(1)->orderBy('front_priority','desc')->orderBy('rate','desc');
    }
	
    public function scopeOption($query, $ids)
    {
        return $query->where(function ($query) use ($ids) {
			foreach($ids as $id){
				$query->whereHas('options', function ($query) use ($id) {
					$query->whereId($id);
				});
			}
		});
    }
}
