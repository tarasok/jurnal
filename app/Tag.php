<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	use Slugable;
	
    protected $table = 'tags';
    protected $fillable = ['alias'];
    
    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_tag', 'tag_id', 'post_id')->withTimestamps();
    }
}
