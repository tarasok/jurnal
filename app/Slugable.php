<?php

namespace App;
use Cache;

trait Slugable
{
    public function scopeFindBySlug($query, $slug)
    {
	return $query->whereAlias($slug);
    }
    
    public function getUrlAttribute()
    {
        if($this->alias){
			return route('slug',$this->alias);
		}else{
			return route(strtolower(join('', array_slice(explode('\\', __CLASS__), -1))),$this->id);
		}
    }
    
    static function url($id, $for_sync = false)
    {
		if($for_sync){ // without cache and hostnames. must be delete
			return '/'.self::whereId($id)->value('alias');
		}else{
			$url = Cache::remember(__CLASS__.'_url_'.$id, 15, function() use ($id) {
				return route('slug',self::whereId($id)->value('alias'));
			});
			return $url;
		}
    }
}