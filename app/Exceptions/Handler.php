<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Response;
use Illuminate\Routing\UrlGenerator;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {    
        if ($e instanceof NotFoundHttpException)
        {
            $url = \App\Url::Where('url','/'. Request()->path())->first();

            if(empty($url)){
                
                $url = new \App\Url;
                $url->url = '/'. Request()->path();
                
            }elseif($url->redirect){
                
                return response()->redirectTo($url->redirect,301);
            }
            
            if(starts_with($request->headers->get('referer'), Url()->to(''))){
                $url->referer = $request->headers->get('referer');
                $url->view_count += 1;
                $url->save();
            }
        }
	
        return parent::render($request, $e);
    }
}
