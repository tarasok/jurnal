<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $appends = ['url_thumb', 'versions_config'];

    protected $casts = [
        'versions' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function filepath($version)
    {
        return $this->type . '/' . $this->path . '/' . $this->name . '-' . $version . '.' . $this->extention;
    }

    public function getVersionsConfigAttribute()
    {
        if ($config = config('image.versions.' . $this->type)) {

            if (empty($config['full'])) {
                $config['full'] = config('image.versions.default.full');
            }

            if (empty($config['thumb'])) {
                $config['thumb'] = config('image.versions.default.thumb');
            }

            return $config;
        } else {
            return config('image.versions.default');
        }
    }

    public function getUrlAttribute()
    {
        $url = new \stdClass();
        foreach ($this->versions as $version){
            $url->{$version} = url('upload/' . $this->filepath($version));
        }
        return $url;
    }

    public function getUrlThumbAttribute()
    {
        return url('upload/' . $this->filepath('thumb'));
    }

    static public function toggleIsRelated($old_id, $new_id)
    {
        if ($old_id != 0 and $old_id != $new_id) {
            $image = Image::find($old_id);
            $image->is_related = false;
            $image->save();
        }

        if ($new_id != 0 and $old_id != $new_id) {
            $image = Image::find($new_id);
            $image->is_related = true;
            $image->save();
        }
    }

}
