<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'nios_my_tags';
    public $timestamps = false;
    protected $connection = 'palmira';
	
	public function options()
	{
	  return $this->belongsToMany('App\Item', 'nios_my_tags_xref', 'tag_id', 'item_id');
	}
}
