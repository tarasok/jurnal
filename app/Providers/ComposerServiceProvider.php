<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Cache;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('parts.categories', function ($view) {
	    
	    $categories = Cache::remember('categories', 120, function() {
		return \App\Category::where('parent_id',79)->where('state',1)->orderBy('order')->get()->toArray();
	    });
	    
	    $view->with('categories', $categories);
        });
	
        view()->composer('parts.best', function ($view) {
	    
	    $ids = $view->posts->pluck('id')->toArray();
	    $posts = Cache::remember('best', 15, function() use ($ids) {
		return \App\Post::where('state',1)->whereNotIn('id',$ids)->skip(date('g'))->orderBy('view_avg','DESC')->take(9)->get();
	    });
	    
	    $view->with('posts', $posts);
        });
	
        view()->composer('parts.business', function ($view) {
	    
	    $posts = Cache::remember('business', 120, function() {
		return \App\Post::where('state',1)->where('category_id',100)->orderBy('id','DESC')->take(3)->get();
	    });
	    
	    $view->with('posts', $posts);
        });
	
        view()->composer('parts.related', function ($view) {
	    
	    $id = $view->post->id;
	    $category_id = $view->post->category_id;
	    
	    $posts = Cache::remember('related.'.$id, 120, function() use ($id, $category_id) {
		
		$limit = 12;

		$posts_category = \App\Post::where('state',1)
		    ->where('id','!=',$id)
		    ->where('category_id',$category_id)
		    ->orderBy('view_avg','DESC')->take(6)->get();

		$limit -= count($posts_category);

		$posts_best = \App\Post::where('state',1)
		    ->where('id','!=',$id)
		    ->where('category_id','!=',$category_id)
		    ->skip(date('g'))
		    ->orderBy('view_avg','DESC')->take($limit)->get();

		return $posts_category->merge($posts_best)->all();
	    });
	    
	    $view->with('posts', $posts);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
