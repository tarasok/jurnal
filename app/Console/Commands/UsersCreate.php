<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UsersCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$articles = DB::connection('joomla')
            ->table('nios_content')
			->where('created_by_alias','!=','')
            ->get();
        foreach($articles as $article){
            $user = DB::connection('joomla')
                ->table('nios_users')
                ->where('name',$article->created_by_alias)
                ->first();
            
            if($user){
                $user_id = $user->id;
            }else{
                $user_id = DB::connection('joomla')
                    ->table('nios_users')
                    ->insertGetId([
                            'name' => $article->created_by_alias,
                            'email' => uniqid().'@mebeljurnal.ru'
                        ]);
                //echo '!';
            }
            //echo $user_id."\n";
            DB::connection('joomla')
                ->table('nios_content')
                ->where('id', $article->id)
                ->update(['created_by' => $user_id, 'created_by_alias' => '']);
        }
    }
}
