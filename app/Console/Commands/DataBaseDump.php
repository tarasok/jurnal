<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DataBaseDump extends Command
{
    protected $signature = 'db:dump';
    protected $description = 'DataBase Dump to database/last.sql';

    public function handle()
    {
        $pass_if_pass =  config('database.connections.mysql.password') ?
            ' -p'.config('database.connections.mysql.password').' ' : ' ';
        
        exec("mysqldump -u".config('database.connections.mysql.username').$pass_if_pass.
            config('database.connections.mysql.database')." > database/last.sql");
    }
}
