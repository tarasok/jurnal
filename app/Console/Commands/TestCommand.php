<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;

class TestCommand extends Command
{
    protected $signature = 'test';
    protected $description = 'Command for development';

    public function handle()
    {
		set_time_limit(30);
		
		$alias = 'shkaf';
		
		$union2 = DB::table('posts')->select('id','alias')
			->addSelect(DB::raw("'post' as `type`"))
			->where('alias',$alias);

		$union1 = DB::table('categories')->select('id','alias')
			->addSelect(DB::raw("'category' as `type`"))
			->where('alias',$alias)
            
			->union($union2)
            ->first();
			//->toSql();
		
	echo '<pre>' . print_r($union1, true) . '</pre>';
		
	}
}
