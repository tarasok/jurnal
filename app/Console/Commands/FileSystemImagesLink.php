<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FileSystemImagesLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'filesystem:images-link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "public/upload" to old_images_path (.env)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!file_exists(env('OLD_IMAGES_PATH'))) {
            return $this->error('The "'.env('OLD_IMAGES_PATH').'" directory das not exists.');
        }

        if (file_exists(public_path('images'))) {
            return $this->error('The "'.public_path('images').'" directory already exists.');
        }

        exec('ln -s '.env('OLD_IMAGES_PATH'). ' ' . public_path('images'));

        $this->info('The "'.public_path('images').'" directory has been linked.');
    }
}
