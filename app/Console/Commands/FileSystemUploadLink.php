<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FileSystemUploadLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'filesystem:upload-link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "public/upload" to "storage/app/public"';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(public_path('upload'))) {
            return $this->error('The "'.public_path('upload').'" directory already exists.');
        }

        exec('ln -s '.storage_path('app/upload'). ' ' . public_path('upload'));

        $this->info('The "'.public_path('upload').'" directory has been linked.');
    }
}
