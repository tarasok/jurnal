<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncPosts extends Command
{
    protected $signature = 'sync:posts {id?}';
    protected $description = 'Synchronizations posts width old database structure';

    public function handle()
    {
		$id = $this->argument('id');
	
		$articles = DB::connection('joomla')->table('nios_content as c')
			->leftJoin('nios_content_shares as s', 'c.id', '=', 's.content_id')
			->where(function ($query) use ($id){
					if(isset($id)){
				$query->whereId($id);
			}
				})
				->select(
			'c.*', 
			's.count as share_count'
			)
				->get();

		$bar = $this->output->createProgressBar(count($articles));

		foreach($articles as $article){

            $post = \App\Post::find($article->id);
            if (empty($post)) {
                $post = new \App\Post;
                $post->id = $article->id;
            }

            if ($article->fulltext and $article->introtext) {
                $post->text = '<div class="lead">' . $article->introtext . "</div>\n" . $article->fulltext;
            } else {
                $post->text = $article->introtext . $article->fulltext;
            }
            $post->text = str_replace('src="images/', 'src="/images/', $post->text);
            $post->text = $this->linksConvert($post->text);

			$post->title = $article->title;
			$post->description = $article->metadesc;
			$post->alias = $article->alias;
			//$post->state = $article->state;
            //$post->category_id = $article->catid;
            $post->created_at = $article->created != '0000-00-00 00:00:00' ? $article->created : null;
            //$post->user_id = $article->created_by;
            //$post->user_alias = $article->created_by_alias;
			//$post->image_thumb = json_decode($article->images)->image_intro ?? '';
			//$post->image_full = json_decode($article->images)->image_fulltext ?? '';
			//$post->view_count = $article->hits;
			//$post->share_count = $article->share_count ?? 0;
			//$post->robots = (isset(json_decode($article->metadata)->robots) and json_decode($article->metadata)->robots == 'noindex, nofollow' )?0:1;
			$post->save();

			$post->tags()->detach();
			$tags = explode(',', $article->metakey);
			$tags_ids = [];

			foreach($tags as $name){
                $name = trim($name);
                if($name){
                    $tag = \App\Tag::whereName($name)->first();
                    if(empty($tag)){
                        $tag = new \App\Tag;
                        $tag->name = $name;
                        $tag->save();
                    }
                    if(!in_array($tag->id,$tags_ids)){
                        $tags_ids[] = $tag->id;
                    }
                }
			}
			if($tags_ids){
                $post->tags()->attach($tags_ids);
			}

			$bar->advance();
		}
		$bar->finish();
		$this->info("\nСинхронизированно статей: ".count($articles));
    }
	
	function linksConvert($text){
		
		preg_match_all('/href=["\']?([^"\'>]+)["\']?/', $text, $matches);
		
		$links_old = $matches[1];
		$links_new = [];
		
		foreach($links_old as $link){
			$link_tmp = $link;
			$link = html_entity_decode($link);
			$info = parse_url($link);
			if(!isset($info['host'])){
				if(isset($info['query'])){
					parse_str($info['query'], $output);
					if(($output['view']??'') == 'article'){
						if (isset($output['id']) and $output['id']) {
							$link_tmp = \App\Post::url($output['id'],true); // without cache and hostnames
						}
					}
				}
			}
			$links_new[] = $link_tmp;
		}
		
		$text = str_replace($links_old, $links_new, $text);
		
		return $text;
	}	
	
}
