<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'url_yml',
        'state',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function price()
    {
        return $this->_price ?? $this->_price = new Price($this);
    }
}
