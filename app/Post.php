<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use carbon\carbon;
use App\Slugable;
use Cache;

class Post extends Model
{
    use Slugable;
    
    protected $table = 'posts';
    protected $appends = ['url','thumb','date'];
    protected $fillable = [
        'title',
        'user_id',
        'user_alias',
        'state',
        'category_id',
        'robots',
        //'text',
        'image_id',
        'header_id'
    ];
    protected $with = ['category','image'];
    protected $visible = ['id','title','url','thumb','category_id','view_count','share_count','view_avg','photo_count','video_count','date','category'];
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }	
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'post_tag', 'post_id', 'tag_id')->withTimestamps();
    }

    public function images()
    {
        return $this->belongsToMany('App\Image', 'post_image', 'post_id', 'image_id')->withTimestamps();
    }

    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }

    public function header()
    {
        return $this->hasOne('App\Image', 'id', 'header_id');
    }

    public function setImageIdAttribute($value)
    {
        Image::toggleIsRelated($this->attributes['image_id'], $value);
        $this->attributes['image_id'] = $value;
    }

    public function setHeaderIdAttribute($value)
    {
        Image::toggleIsRelated($this->attributes['header_id'], $value);
        $this->attributes['header_id'] = $value;
    }
    
    public function getThumbAttribute()
    {
        if(isset($this->image->id)){
            return $this->image->url->full;
        }else{
            return url($this->image_thumb);
        }
    }

    public function getHeaderUrlAttribute()
    {
        if(isset($this->header->id)){
            return $this->header->url->full;
        }else{
            return url($this->image_full);
        }
    }
    
    public function getDateAttribute()
    {
        return $this->created_at?$this->created_at->format('d.m.Y'):'---';
    }
    
    public function getViewAvgAttribute($value)
    {
        return round($value);
    }
	
    public function scopeSearch($query, $phrase)
    {
		
		if(is_numeric($phrase)){
			return $query->where('id',$phrase);
		}
		
		$words = explode(' ',$phrase);
		
        $query->where(function ($query) use ($words) {
				$fields  = ['title','text','description','user_alias'];
				foreach($fields as $field){
					$query->orWhere(function ($query) use ($words, $field) {
						foreach($words as $word){
							$query->where($field,'LIKE','%'.$word.'%');
						}
					});
				}
			});
		
		$query->orWhereHas('user', function ($query) use ($words) {
				foreach($words as $word){
					$query->where('name','LIKE','%'.$word.'%');
				}
			});
			
		$query->orWhereHas('category', function ($query) use ($words) {
				foreach($words as $word){
					$query->where('title','LIKE','%'.$word.'%');
				}
			});
			
		return $query;
    }
    public function scopePublished($query)
    {
		$query->where('state',1)
              ->whereHas('category', function ($query) {
                    $query->where('state', 1);
              });
			
		return $query;
    } 
}
