<?php

namespace App\Observers;
use App\Category;
use Cache;

class CategoryObserver
{
    public function saving(Category $category)
    {
        Cache::flush();
    }
}