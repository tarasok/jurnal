<?php

namespace App\Observers;
use App\Post;
use Carbon\Carbon;
use Cache;

class PostObserver
{    
    public function creating(Post $post)
    {
        $post->user_id = 1190;
    }

    public function saving(Post $post)
    {
        if($post->created_at and $post->created_at->diffInDays(Carbon::now())){
            $post->view_avg = $post->view_count/$post->created_at->diffInDays();
        }
        $post->photo_count = substr_count($post->text, '<img ');
        $post->video_count = substr_count($post->text, '{youtube ');
    }

    public function saved(Post $post)
    {
		$user = $post->user;
		$user->post_count = $user->posts()->published()->count();
		$user->post_view_count = $user->posts()->published()->sum('view_count');
		$user->share_count = $user->posts()->published()->sum('share_count');
		$user->view_avg = $user->posts()->published()->sum('view_avg');
		$user->photo_count = $user->posts()->published()->sum('photo_count');
		$user->video_count = $user->posts()->published()->sum('video_count');
		$user->rating	= ($user->post_view_count * $user->share_count) / 100000
						+ $user->photo_count
						+ $user->video_count * 10
						+ $user->post_count * 10;
		$user->save();

        $post->tags->each(function ($tag) {
            $tag->post_count = $tag->posts()->published()->count();
            $tag->save();
        });

        Cache::flush();
    }
}