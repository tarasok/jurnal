<?php

namespace App\Observers;
use App\User;
use Cache;

class UserObserver
{    
    public function saving(User $user)
    {
        Cache::flush();
    }
}