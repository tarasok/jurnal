<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('rating')->after('remember_token');
            $table->integer('video_count')->after('photo_count');
        });
		
        Schema::table('posts', function (Blueprint $table) {
            $table->integer('video_count')->after('photo_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('rating');
            $table->dropColumn('video_count');
        });
		
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('video_count');
        });
    }
}
