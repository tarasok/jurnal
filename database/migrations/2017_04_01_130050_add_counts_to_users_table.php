<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('post_count')->after('view_count');
            $table->integer('photo_count')->after('view_count');
            $table->integer('share_count')->after('view_count');
            $table->integer('view_avg')->after('view_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('post_count');
            $table->dropColumn('photo_count');
            $table->dropColumn('share_count');
            $table->dropColumn('view_avg');
        });
    }
}
