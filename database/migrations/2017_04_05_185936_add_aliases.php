<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAliases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('alias')->after('email');
        });
		
        Schema::table('tags', function (Blueprint $table) {
            $table->string('alias')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['alias']);
        });
		
        Schema::table('tags', function (Blueprint $table) {
            $table->dropColumn(['alias']);
        });
    }
}
