<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'versions' => [

        'default' => [
            'full' => [
                'method' => 'resize',
                'width' => 800,
                'height' => 800,
            ],
            'thumb' => [
                'method' => 'fit',
                'width' => 100,
                'height' => 100,
            ],
        ],

        //  extends 'default'

        'user_avatar' => [
            'full' => [
                'method' => 'fit',
                'width' => 250,
                'height' => 250,
            ],
            'small' => [
                'method' => 'fit',
                'width' => 50,
                'height' => 50,
            ],
        ],

        'user_background' => [
            'full' => [
                'method' => 'original',
            ],
            'thumb' => [
                'method' => 'resize',
                'width' => 100,
                'height' => 100,
            ],
        ],

        'post_image' => [
            'full' => [
                'method' => 'fit',
                'width' => 800,
                'height' => 600,
            ],
            'thumb' => [
                'method' => 'fit',
                'width' => 100,
                'height' => 75,
            ],
        ],

        'post_header' => [
            'full' => [
                'method' => 'original',
            ],
            'thumb' => [
                'method' => 'resize',
                'width' => 100,
                'height' => 100,
            ],
        ],

    ]

);
